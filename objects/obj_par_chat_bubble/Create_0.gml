/// README
/*
	Each character that uses the chat bubble must place this object as its parent.
	The following items need to be initialized in the Create event of the object:
	1. myChatBubble = noone;
	2. myObj = the object this chat bubble is speaking to
	3. if there exists no object collision, specify a condition as myCondition
	4. myText[0] = "Sample Text"; (Feel free to add as many text items as you need)
*/
