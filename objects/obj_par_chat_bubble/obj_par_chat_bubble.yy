{
    "id": "24605439-1e05-49db-9627-9c2a0e6e7711",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_par_chat_bubble",
    "eventList": [
        {
            "id": "fe70a5e4-d5ea-4dd7-99cb-25213abc89ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "24605439-1e05-49db-9627-9c2a0e6e7711"
        },
        {
            "id": "2154d5f6-c1a2-4edd-93d9-a0414c4e6341",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "24605439-1e05-49db-9627-9c2a0e6e7711"
        },
        {
            "id": "3a37ae21-bd12-4cfb-b141-12d28867b78b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "24605439-1e05-49db-9627-9c2a0e6e7711"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}