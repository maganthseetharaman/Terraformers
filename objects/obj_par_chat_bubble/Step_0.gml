/// @description Insert description here
if(place_meeting(x,y,myObj) || myCondition == true)
{
	if(myChatBubble == noone)
	{
		myChatBubble = instance_create_layer(x,y,"Text",obj_chat_bubble);
		myChatBubble.cb_text = myText;
		myChatBubble.creator = self;
	}
}
else
{
	if(myChatBubble != noone)
	{
		instance_destroy(myChatBubble);
		myChatBubble = noone;
	}
}
