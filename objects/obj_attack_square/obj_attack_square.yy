{
    "id": "51f2dfb7-c31e-40fd-83e7-6adcf39858ec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_attack_square",
    "eventList": [
        {
            "id": "ad1adf93-d6f2-46b2-99a8-7f9e1a2d8339",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "51f2dfb7-c31e-40fd-83e7-6adcf39858ec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d746718f-f068-45ea-9872-acfb7a713c23",
    "visible": true
}