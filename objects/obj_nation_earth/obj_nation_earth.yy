{
    "id": "6d0c0871-f58d-4be2-8879-360a7c50cb7d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_nation_earth",
    "eventList": [
        {
            "id": "6634f269-1302-43f5-9bf1-d39548874cb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6d0c0871-f58d-4be2-8879-360a7c50cb7d"
        },
        {
            "id": "f34e81ee-2c53-4c0a-baa4-df8ee8ee720c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6d0c0871-f58d-4be2-8879-360a7c50cb7d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "df8adc3f-baa3-40f0-8c9f-ab0436fe3177",
    "visible": true
}