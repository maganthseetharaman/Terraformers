{
    "id": "4be7101c-ba90-4671-9e4a-c24dfe28be1a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_air_button",
    "eventList": [
        {
            "id": "80f332a2-2c81-48d6-a010-94cb9b5bcb8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "4be7101c-ba90-4671-9e4a-c24dfe28be1a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87c530ab-8b5f-4c32-819b-4ea47a4814de",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "be23e64e-30bf-4e23-9630-aee6b2e4bfda",
    "visible": true
}