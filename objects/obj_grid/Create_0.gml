/// Create the grid
grid_left = 64;
grid_top = 96;
grid_rows = 7;
grid_columns = 17;
grid_cell_size = 32;

global.map_grid = mp_grid_create(grid_left,grid_top,grid_columns,grid_rows,grid_cell_size,grid_cell_size);
global.navigate = path_add();

global.boundry_top = grid_top - grid_cell_size;
global.boundry_bottom = global.boundry_top + (grid_cell_size * (grid_rows + 1));
global.boundry_left = grid_left - grid_cell_size;
global.boundry_right = global.boundry_left + (grid_cell_size * (grid_columns + 1));
global.move_sqaure_distance = 1;