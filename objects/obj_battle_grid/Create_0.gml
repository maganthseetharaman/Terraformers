/// Create the grid
grid_left = 64;
grid_top = 128;
grid_rows = 6;
grid_columns = 15;
//change made 128 -> 32
global.grid_cell_size = 32;

global.map_grid = mp_grid_create(grid_left,grid_top,grid_columns,grid_rows,global.grid_cell_size,global.grid_cell_size);
global.navigate = path_add();

global.boundry_top = grid_top - global.grid_cell_size;
global.boundry_bottom = global.boundry_top + (global.grid_cell_size * (grid_rows + 1));
global.boundry_left = grid_left - global.grid_cell_size;
global.boundry_right = global.boundry_left + (global.grid_cell_size * (grid_columns + 1));
global.move_sqaure_distance = 1;

global.selectedPlayer = noone;
global.selectedEnemy = noone;
global.move_state = true;
global.attacked = false;
global.team_turn = obj_player_1;

global.movement_point = 1;

//Attack Point Requirement
global.fire_attack = 10;
global.water_attack = 10;
global.air_attack = 10;
global.earth_attack = 10;

//Attack Damage
global.fire_damage = 10;
global.water_damage = 10;
global.air_damage = 10;
global.earth_damage = 10;