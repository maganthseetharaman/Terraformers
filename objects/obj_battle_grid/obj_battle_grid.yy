{
    "id": "95c4eb92-6f22-4a59-9818-27d57e2abd76",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_battle_grid",
    "eventList": [
        {
            "id": "b6ba2dee-51b9-4f9c-86a4-afe00ff20b09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "95c4eb92-6f22-4a59-9818-27d57e2abd76"
        },
        {
            "id": "44c78abb-954b-48ee-b61e-d3f2796105a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "95c4eb92-6f22-4a59-9818-27d57e2abd76"
        },
        {
            "id": "fdd271b7-b720-4aa0-beef-829580ab46e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "95c4eb92-6f22-4a59-9818-27d57e2abd76"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}