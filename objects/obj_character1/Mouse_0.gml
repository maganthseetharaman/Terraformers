if(global.selected == noone) return;
if(global.selected.movement_points <= 0)return;

if(instance_exists(obj_movement_point_indicator)) { instance_destroy(obj_movement_point_indicator) }
instance_create_layer(movement_point_message_x, movement_point_message_y, rm_arena, obj_movement_point_indicator);

for(i = -global.move_sqaure_distance; i <= global.move_sqaure_distance; i++) {
	for(j = -global.move_sqaure_distance; j <= global.move_sqaure_distance; j++) {
		if(i != 0) || (j != 0) {
			var x_offset = x + (i * 32);
			var y_offset = y + (j * 32);
		
			if(x_offset  <= global.boundry_left) || (x_offset >= global.boundry_right) continue;
			if(y_offset <= global.boundry_top) || (y_offset >= global.boundry_bottom) continue;
			if(instance_position(x_offset, y_offset, obj_character1)) continue;
			if(instance_position(x_offset, y_offset, obj_enemy1)) {
				instance_create_layer(x_offset, y_offset, rm_arena, obj_attack_square);	
				continue;
			}
			
			instance_create_layer(x_offset, y_offset, rm_arena, obj_move_square);
		}
	}
}