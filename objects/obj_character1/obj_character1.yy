{
    "id": "27952ec6-22d1-4482-b189-9a9d10d7239f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_character1",
    "eventList": [
        {
            "id": "74967355-4e55-4111-9cb1-bf7643c3e345",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "27952ec6-22d1-4482-b189-9a9d10d7239f"
        },
        {
            "id": "95e79622-cd08-4e17-8945-b1fd962d3a93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "27952ec6-22d1-4482-b189-9a9d10d7239f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b7da91b1-b533-47e5-af9f-0002a748363d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "2e3a00d5-88c5-47a4-a608-a20b0111f438",
    "visible": true
}