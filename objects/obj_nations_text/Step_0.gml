/// @description Insert description here
if(keyboard_check_pressed(vk_enter))
{
	switch(hit_enter)
	{
		case 0:
			myTextbox_E = instance_create_layer(0, 0, "Text", obj_textbox);
			myTextbox_E.tb_text = "This is the Earth Kingdom.";
			hit_enter += 1;
			break;
		case 1:
			myTextbox_A = instance_create_layer(480, 290, "Text", obj_textbox);
			myTextbox_A.tb_text = "This is the Air Kingdom.";
			hit_enter += 1;
			break;
		case 2:
			myTextbox_F = instance_create_layer(0, 290, "Text", obj_textbox);
			myTextbox_F.tb_text = "This is the Fire Kingdom.";
			hit_enter += 1;
			break;
		case 3:
			myTextbox_W = instance_create_layer(480, 0, "Text", obj_textbox);
			myTextbox_W.tb_text = "This is the Water Kingdom.";
			hit_enter += 1;
			break;
	}
}
