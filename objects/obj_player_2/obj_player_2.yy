{
    "id": "7c4be6e9-d277-499a-a65d-03539220f778",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_2",
    "eventList": [
        {
            "id": "5acd1b24-a218-4806-b9cb-7a9ea1fb47f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7c4be6e9-d277-499a-a65d-03539220f778"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f94ca183-c510-4027-a4bc-dbebe8e449bb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8ca10948-ffb5-4dfd-83fa-e9e76b7fe426",
    "visible": true
}