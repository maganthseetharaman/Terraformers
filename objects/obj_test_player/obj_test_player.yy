{
    "id": "616bb922-9cc8-4564-9423-eeb5519d5a13",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_test_player",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "978468ba-ad27-4f63-a724-636ec6713ee0",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7b50f08f-58d7-4235-80c6-5fd51474ca89",
    "visible": true
}