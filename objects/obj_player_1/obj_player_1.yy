{
    "id": "99a3e290-0100-4ffd-ab60-845a40676691",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_1",
    "eventList": [
        {
            "id": "47a9653e-5d5f-46d8-b960-f8d71be9debe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "99a3e290-0100-4ffd-ab60-845a40676691"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f94ca183-c510-4027-a4bc-dbebe8e449bb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8ca10948-ffb5-4dfd-83fa-e9e76b7fe426",
    "visible": true
}