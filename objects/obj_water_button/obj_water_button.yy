{
    "id": "bef6212c-eb72-431b-ab2c-713b2fc5e4e8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_water_button",
    "eventList": [
        {
            "id": "892dca61-48ef-4ae4-9380-fe7608efe47f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "bef6212c-eb72-431b-ab2c-713b2fc5e4e8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87c530ab-8b5f-4c32-819b-4ea47a4814de",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "96880c5f-ba51-4e48-bdf4-b6f3222c4986",
    "visible": true
}