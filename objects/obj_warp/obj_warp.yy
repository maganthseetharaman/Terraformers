{
    "id": "46b1c942-e6b7-4d1c-9d49-4b5efd7db5c5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_warp",
    "eventList": [
        {
            "id": "46ae2227-ec0d-457a-bd15-df5b569bf55c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "46b1c942-e6b7-4d1c-9d49-4b5efd7db5c5"
        },
        {
            "id": "7d135b87-3ba2-4fc4-8309-b2f856afe6b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "616bb922-9cc8-4564-9423-eeb5519d5a13",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "46b1c942-e6b7-4d1c-9d49-4b5efd7db5c5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dccb3e1b-ec7c-442e-9582-0b0f20d74ffc",
    "visible": true
}