var attack_buttons_layer = layer_get_id("attack_buttons");
var button_grid_start_x = 288;
var button_grid_start_y = 320;

global.selectedEnemy = instance_position(x, y, par_player);
attack_buttons_layer = layer_get_id("attack_buttons");
	
instance_create_layer(button_grid_start_x + (0 * global.grid_cell_size), button_grid_start_y, attack_buttons_layer, obj_fire_button);
instance_create_layer(button_grid_start_x + (1 * global.grid_cell_size), button_grid_start_y, attack_buttons_layer, obj_water_button);
instance_create_layer(button_grid_start_x + (2 * global.grid_cell_size), button_grid_start_y, attack_buttons_layer, obj_air_button);
instance_create_layer(button_grid_start_x + (3 * global.grid_cell_size), button_grid_start_y, attack_buttons_layer, obj_earth_button);

with(global.selectedPlayer) {
	
	if(global.move_state == false) return;
	global.move_state = false;
}
