{
    "id": "e48e23a7-419f-42b8-86d5-10099a1143e2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_square",
    "eventList": [
        {
            "id": "575fa51f-d9f0-4abf-9658-d82f0a1ba685",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "e48e23a7-419f-42b8-86d5-10099a1143e2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "caeb631e-4c6d-4b87-b003-8c248fc3201b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fad4de05-f9d3-4166-bf3f-1cd0da6385e0",
    "visible": true
}