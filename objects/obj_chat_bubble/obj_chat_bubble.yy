{
    "id": "8d2bc81a-0a3e-46d6-be4b-ba84c9e999ae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_chat_bubble",
    "eventList": [
        {
            "id": "1918e863-2a62-4c90-a3ac-d81f2d7b0a70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8d2bc81a-0a3e-46d6-be4b-ba84c9e999ae"
        },
        {
            "id": "3cecc808-260c-4819-be7b-0a8c89d59acd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8d2bc81a-0a3e-46d6-be4b-ba84c9e999ae"
        },
        {
            "id": "334651f4-10dd-4006-a31f-cc862ac7dfb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8d2bc81a-0a3e-46d6-be4b-ba84c9e999ae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}