/// @description Insert description here
// draw chat bubble
draw_sprite(spr_chat_bubble,0,x,y);

//draw text
draw_set_color(c_black);
draw_set_font(fnt_chat_bubble);

//draw text part by part 
text_part = string_copy(cb_text[page],0,part_count);
if(part_count < string_length(cb_text[page]))
{
	part_count += 1;
}

draw_text_ext(x - x_buf,y - y_buf,text_part,sep_height - sep_val,box_width - box_buf);
