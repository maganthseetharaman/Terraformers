var move_square_x = x;
var move_square_y = y;

with(global.selectedPlayer) {
	if(global.move_state == false) return;
	global.move_state = false;
	
	scr_navigation(x, y, move_square_x, move_square_y, movement_speed);
	movement_points -= global.movement_point;
	
	
	//Every player instance other than one currently moving has 
	//random 50% chance of gaining an extra movement point
	with(par_player) {
		if (global.selectedPlayer != self.id) && (global.team_turn == self.object_index) &&(random_range(0, 1) > 0.5)
			self.movement_points += 1;
	}
}