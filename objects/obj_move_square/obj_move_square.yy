{
    "id": "25c49168-ab56-4ec4-86a5-6fa3e9fe87c7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_move_square",
    "eventList": [
        {
            "id": "fd3a30d6-e424-439d-a3e9-37510cb7080a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "25c49168-ab56-4ec4-86a5-6fa3e9fe87c7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "caeb631e-4c6d-4b87-b003-8c248fc3201b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "46fc482d-21fd-4512-943b-78f0e1d85ea6",
    "visible": true
}