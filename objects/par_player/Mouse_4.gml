if(instance_position(x, y, obj_enemy_square)) return;
if(instance_exists(obj_move_square)) instance_destroy(obj_move_square);
if(instance_exists(obj_enemy_square)) instance_destroy(obj_enemy_square);
if(instance_exists(obj_move_point_indicator)) instance_destroy(obj_move_point_indicator);
if(instance_exists(par_attack_button)) instance_destroy(par_attack_button);

var player = instance_nearest(x,y,par_player);

if(global.selectedPlayer == player) || (global.move_state == false) {
	global.move_state = true;
	global.selectedPlayer = noone;
	return;
}
else if (instance_position(x, y, obj_enemy_square)) && (global.team_turn != player.object_index) {
	event_perform_object(obj_enemy_square, ev_mouse, ev_left_press);
	return;
}
else if (global.team_turn == player.object_index) {
	global.move_state = true;
	global.selectedPlayer = player;
}

else {
	global.selectedPlayer = noone;
	return;	
}

if(global.selectedPlayer.movement_points <= 0) return;

for(i = -global.move_sqaure_distance; i <= global.move_sqaure_distance; i++) {
	for(j = -global.move_sqaure_distance; j <= global.move_sqaure_distance; j++) {
		if(i != 0) || (j != 0) {
			var x_offset = x + (i * global.grid_cell_size);
			var y_offset = y + (j * global.grid_cell_size);
		
			if(x_offset  <= global.boundry_left) || (x_offset >= global.boundry_right) continue;
			if(y_offset <= global.boundry_top) || (y_offset >= global.boundry_bottom) continue;
			var opposite_team = global.team_turn == obj_player_1 ? obj_player_2 : obj_player_1;
			if(instance_position(x_offset, y_offset, opposite_team)) {
				instance_create_layer(x_offset, y_offset, enemy_square_layer, obj_enemy_square);
			}
			
			instance_create_layer(x_offset, y_offset, square_layer, obj_move_square);
		}
	}
}