//POINT SYSTEM
health_points = 10;
movement_points = 10;
attack_points = 10;

movement_speed = 10;

square_layer = layer_get_id("move_squares");
enemy_square_layer = layer_get_id("enemy_squares");
indicator_layer_id = layer_get_id("indicators");

player_sqaure = instance_create_layer(x, y, square_layer, obj_player_square);