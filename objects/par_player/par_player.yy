{
    "id": "f94ca183-c510-4027-a4bc-dbebe8e449bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "par_player",
    "eventList": [
        {
            "id": "b2de54a9-d8ab-4f3b-aa71-db51bd3a5e95",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f94ca183-c510-4027-a4bc-dbebe8e449bb"
        },
        {
            "id": "932a4408-1704-49b9-a99e-a6d3b05b8019",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "f94ca183-c510-4027-a4bc-dbebe8e449bb"
        },
        {
            "id": "0a1356a9-0100-4204-bb8b-7c4e447a56bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f94ca183-c510-4027-a4bc-dbebe8e449bb"
        },
        {
            "id": "45c75135-9d34-4984-a38a-3e8730874d57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "f94ca183-c510-4027-a4bc-dbebe8e449bb"
        },
        {
            "id": "6d6aa10a-087f-4471-874f-f79d7a5f03d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "f94ca183-c510-4027-a4bc-dbebe8e449bb"
        },
        {
            "id": "cd542cbd-56c6-4bd3-9574-9cbf983ce279",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "f94ca183-c510-4027-a4bc-dbebe8e449bb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}