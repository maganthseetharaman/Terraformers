draw_set_font(fnt_textbox);
draw_text(x+50, y+50, "TEAM TURN ")
var team_turn_sprite = global.team_turn == obj_player_1 ? spr_team1_square : spr_team2_square
draw_sprite_ext(team_turn_sprite, 0, x+40, y+50, image_xscale*0.25, image_yscale*0.25, image_angle, image_blend, image_alpha);