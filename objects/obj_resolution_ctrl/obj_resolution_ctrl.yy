{
    "id": "d4a0ac8f-f400-439a-b81a-503483bccba5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_resolution_ctrl",
    "eventList": [
        {
            "id": "f7784961-5ae7-4a5f-8bb5-a7661a9d9f86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d4a0ac8f-f400-439a-b81a-503483bccba5"
        },
        {
            "id": "28717389-2e03-46d1-9540-534028bcd02f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d4a0ac8f-f400-439a-b81a-503483bccba5"
        },
        {
            "id": "6f0022e2-67b7-432f-8063-0e1d3238539f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d4a0ac8f-f400-439a-b81a-503483bccba5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}