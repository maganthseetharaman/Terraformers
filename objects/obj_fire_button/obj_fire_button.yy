{
    "id": "d7a800aa-fd17-44bf-a46f-d1bcb4c5bfef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fire_button",
    "eventList": [
        {
            "id": "28d4f1f6-007b-4be7-a0c0-8a337b67efcc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d7a800aa-fd17-44bf-a46f-d1bcb4c5bfef"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87c530ab-8b5f-4c32-819b-4ea47a4814de",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ba01b4a4-d645-4617-a65f-1cee92b58d2b",
    "visible": true
}