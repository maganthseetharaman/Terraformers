{
    "id": "2eeb0b0d-130a-441f-a26b-74703ed1fe1b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_res_ctrl",
    "eventList": [
        {
            "id": "1a70178b-967b-4665-93c4-a157c960271f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2eeb0b0d-130a-441f-a26b-74703ed1fe1b"
        },
        {
            "id": "bd592058-4cf1-44c1-9507-aece51f69105",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2eeb0b0d-130a-441f-a26b-74703ed1fe1b"
        },
        {
            "id": "4cddb799-68c2-4442-b2a1-f1b1a3514468",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2eeb0b0d-130a-441f-a26b-74703ed1fe1b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}