var move_square_x = x;
var move_square_y = y;

with(global.selectedPlayer) {
	if(((x == move_square_x) && (y == move_square_y)) || (global.attacked == true)) {
		global.selectedPlayer = noone;
		global.move_state = true;
		global.attacked = false;
		
		if(!instance_exists(obj_player_1)) global.team_turn = obj_player_2;
		else if(!instance_exists(obj_player_2)) global.team_turn = obj_player_1;
		else global.team_turn = global.team_turn == obj_player_1 ? obj_player_2 : obj_player_1;
			
		if(instance_exists(obj_enemy_square)) instance_destroy(obj_enemy_square);
		
		//To destroy current instance after executing all above instancce
		if(instance_exists(obj_move_square)) instance_destroy(obj_move_square);
	}
}