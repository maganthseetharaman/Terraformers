{
    "id": "713651c9-875a-4e9f-96b7-b43d95f49145",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy1",
    "eventList": [
        {
            "id": "399f665b-daff-44da-9612-f90a53a67eea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "713651c9-875a-4e9f-96b7-b43d95f49145"
        },
        {
            "id": "69e5aba0-bf1c-495e-a594-bd4231a474cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "713651c9-875a-4e9f-96b7-b43d95f49145"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9a4a2f8e-dc93-4e75-9374-c4a148f79464",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2e3a00d5-88c5-47a4-a608-a20b0111f438",
    "visible": true
}