{
    "id": "cd66ef4f-58fb-4e42-9519-39f6a76dfecb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_guide_character",
    "eventList": [
        {
            "id": "e57f8608-d5dd-4f3f-b0d3-6f72ea4b6c6a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cd66ef4f-58fb-4e42-9519-39f6a76dfecb"
        },
        {
            "id": "397fa0d0-4cf5-48d7-b9dc-e09defa5578b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cd66ef4f-58fb-4e42-9519-39f6a76dfecb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "24605439-1e05-49db-9627-9c2a0e6e7711",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "652cc66b-87c2-49b0-bf7f-d3219473a173",
    "visible": true
}