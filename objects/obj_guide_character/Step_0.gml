/// @description Insert description here

//initially player isn't visible
obj_test_player.visible = false;

if(keyboard_check_pressed(vk_enter) && hit_enter < 4)
{
	myCondition = false;
	hit_enter += 1;
}
if(hit_enter == 4)
{
	myCondition = true;
	myText[0] = "Press <space> to see what NPCs and other characters have to say. Try it out now.";
	myText[1] = "Use the F2 and F3 keys to shrink and zoom resolution.";
	myText[2] = "Use the arrow keys for navigation in the player's normal speed.";
	myText[3] = "Press <shift> + <arrows> to help your character run.";
	myText[4] = "Press <ctrl> + <arrows> to help your character slow down.";
	myText[5] = "Head to the portal at the center of the map to begin your journey.";
	myText[6] = "You're good to go!"
}

//----CHECK CONDITION AND DISPLAY CHAT BUBBLE----

if(place_meeting(x,y,myObj) || myCondition == true || hit_enter == 0)
{
	if(myChatBubble == noone)
	{
		myChatBubble = instance_create_layer(x,y,"Text",obj_chat_bubble);
		myChatBubble.cb_text = myText;
		myChatBubble.creator = self;
	}
	myCondition = false;
}
else
{
	if(myChatBubble != noone)
	{
		instance_destroy(myChatBubble);
		myChatBubble = noone;
	}
}

//----CHARACTER NEEDS TO APPEAR----

if(global.guideEnd == 1)
{
	obj_test_player.visible = true;
}




