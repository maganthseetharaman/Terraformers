{
    "id": "c507327b-3a4b-4c8d-acab-08c7e8ff0097",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_earth_button",
    "eventList": [
        {
            "id": "295fbad7-fd9e-4199-b622-86d4810941b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "c507327b-3a4b-4c8d-acab-08c7e8ff0097"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87c530ab-8b5f-4c32-819b-4ea47a4814de",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b638dbb2-7678-4a55-9a6b-c4e7e75c6646",
    "visible": true
}