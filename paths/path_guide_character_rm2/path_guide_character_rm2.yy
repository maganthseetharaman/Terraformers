{
    "id": "2c64825f-0331-4ef1-89c4-ac13a5965c62",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_guide_character_rm2",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "91176731-f589-4a00-aa2b-77bff5aa8f73",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2304,
            "y": 896,
            "speed": 100
        },
        {
            "id": "5794e5db-0e10-45ab-987b-6a1ce6baf6be",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1696,
            "y": 1056,
            "speed": 100
        },
        {
            "id": "d2749238-6916-491d-a48c-25f18e0ca410",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1632,
            "y": 992,
            "speed": 100
        },
        {
            "id": "b71ebd0d-7f3c-4535-b5ee-6e0ede6f5fb5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1568,
            "y": 960,
            "speed": 100
        },
        {
            "id": "207caf98-9862-4463-aad8-baccd240e611",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1568,
            "y": 832,
            "speed": 100
        },
        {
            "id": "b353f896-fcea-4465-a94d-2aa2a1e41db2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1472,
            "y": 736,
            "speed": 100
        },
        {
            "id": "adadbf39-ef89-475e-aa77-3fd7cd84797e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1280,
            "y": 704,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}