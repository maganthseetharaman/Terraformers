/// @param attack_point_requirement
/// @param damage_point_caused

var attack_point_requirement = argument0;
var damage_point_caused = argument1;

with(global.selectedPlayer) {
	//Every player instance other than one currently moving has 
	//random 50% chance of gaining an extra movement point
	with(par_player) {
		if (global.selectedPlayer != self.id) && (global.team_turn ==  self.object_index) &&(random_range(0, 1) > 0.5)
			self.movement_points += 1;
	}
	
	if(attack_points >= attack_point_requirement) {
		attack_points -= attack_point_requirement;
		
		if(global.selectedEnemy.health_points - damage_point_caused) <= 0 {
			scr_navigation(x, y, global.selectedEnemy.x, global.selectedEnemy.y, movement_speed);
			movement_points -= global.movement_point;
		}
		
		with(global.selectedEnemy) {
			health_points -= damage_point_caused;
			global.attacked = true;
			instance_destroy(par_attack_button);
			if(health_points <= 0) instance_destroy(self.id);
		}
	}
}