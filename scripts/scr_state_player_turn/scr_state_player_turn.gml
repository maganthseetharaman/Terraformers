if(instance_position(mouse_x,mouse_y,par_character1) && mouse_check_button_pressed(mb_left))
{
	var player;
	player = instance_nearest(mouse_x,mouse_y,par_character1)
	
	if(instance_exists(obj_move_square)) { instance_destroy(obj_move_square) }
	if(instance_exists(obj_attack_square)) { instance_destroy(obj_attack_square) }
	
	if(global.selected == player) {
		global.selected = noone;
		return;
	}
	else global.selected = player
}

if(global.selected != noone && mouse_check_button_pressed(mb_right))
{
	if(instance_position(mouse_x, mouse_y, obj_move_square))
	{
		global.moving = true
		
		if(instance_exists(obj_move_square)) { instance_destroy(obj_move_square) }
		if(instance_exists(obj_attack_square)) { instance_destroy(obj_attack_square) }
		
		with(global.selected)
		{
			sprite_index = run_anim
			scr_navigation(x,y,round(mouse_x/34)*32,round(mouse_y/34)*32,1)
			movement_points -= 1
			global.selected = noone
			
		}
	}
	
	else if(instance_position(mouse_x,mouse_y,obj_attack_square))
	{
		global.attacking = true
		with(global.selected)
		{
			if(distance_to_object(obj_attack_square) > 13)
			{
				global.moving = true
				scr_calculate_dist()
				sprite_index = run_anim
				scr_navigation(x,y,round(global.attack_travel_x/32)*32,round(global.attack_travel_y/32)*32,1)
				global.moving = false
			}
		}
		if(instance_exists(obj_move_square)) { instance_destroy(obj_move_square) }
		if(instance_exists(obj_attack_square)) { instance_destroy(obj_attack_square) }
	}
}

if ( global.moving == true)
{
	with(global.selected)
	{
		if(path_index == -1)
		{
			sprite_index = idle_anim
			cur_node_x=x;
			cur_node_y=y;
			global.moving=false;
		}
	}
}

if( global.attacking == true)
{
	if(global.moving == false)
	{
		with(global.selected) { scr_melee_attack() }
	}
}