if(sprite_index != attack_anim)
{
	if(global.enemy.x < x)
	{
		image_xscale = -1
	}
	sprite_index = attack_anim
	image_index = 0
}

if(image_index >= image_number-1)
{
	sprite_index = idle_anim
	global.attacking = false
	image_xscale = 1 
	with(obj_enemy1) { health_point -= 10; }
}