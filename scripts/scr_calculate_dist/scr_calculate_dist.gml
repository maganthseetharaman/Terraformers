var d1,d2,d3,d4,d5,d6,d7,d8,nearest_attack_square

nearest_attack_square = instance_nearest(mouse_x,mouse_y,obj_attack_square)


if(instance_position(nearest_attack_square.x-32,nearest_attack_square.y,obj_move_square)) { d1 = point_distance(global.selected.x,global.selected.y,nearest_attack_square.x-32,nearest_attack_square.y) }
else { d1 = 10000}
if(instance_position(nearest_attack_square.x+32,nearest_attack_square.y,obj_move_square)) { d2 = point_distance(global.selected.x,global.selected.y,nearest_attack_square.x+32,nearest_attack_square.y) }
else { d2 = 10000}
if(instance_position(nearest_attack_square.x-32,nearest_attack_square.y-32,obj_move_square)) { d3 = point_distance(global.selected.x,global.selected.y,nearest_attack_square.x-32,nearest_attack_square.y-32) }
else { d3 = 10000}
if(instance_position(nearest_attack_square.x-32,nearest_attack_square.y+32,obj_move_square)) { d4 = point_distance(global.selected.x,global.selected.y,nearest_attack_square.x-32,nearest_attack_square.y+32) }
else { d4 = 10000}
if(instance_position(nearest_attack_square.x+32,nearest_attack_square.y-32,obj_move_square)) { d5 = point_distance(global.selected.x,global.selected.y,nearest_attack_square.x+32,nearest_attack_square.y-32) }
else { d5 = 10000}
if(instance_position(nearest_attack_square.x+32,nearest_attack_square.y+32,obj_move_square)) { d6 = point_distance(global.selected.x,global.selected.y,nearest_attack_square.x+32,nearest_attack_square.y+32) }
else { d6 = 10000}
if(instance_position(nearest_attack_square.x,nearest_attack_square.y-32,obj_move_square)) { d7 = point_distance(global.selected.x,global.selected.y,nearest_attack_square.x,nearest_attack_square.y-32) }
else { d7 = 10000}
if(instance_position(nearest_attack_square.x,nearest_attack_square.y+32,obj_move_square)) { d8 = point_distance(global.selected.x,global.selected.y,nearest_attack_square.x,nearest_attack_square.y+32) }
else { d8 = 10000}

dest = min(d1,d2,d3,d4,d5,d6,d7,d8)

if(dest == d1)
{
	global.attack_travel_x = nearest_attack_square.x - 32 
	global.attack_travel_y = nearest_attack_square.y
}
if(dest == d2)
{
	global.attack_travel_x = nearest_attack_square.x + 32 
	global.attack_travel_y = nearest_attack_square.y
}
if(dest == d3)
{
	global.attack_travel_x = nearest_attack_square.x - 32 
	global.attack_travel_y = nearest_attack_square.y - 32
}
if(dest == d4)
{
	global.attack_travel_x = nearest_attack_square.x - 32 
	global.attack_travel_y = nearest_attack_square.y + 32
}
if(dest == d5)
{
	global.attack_travel_x = nearest_attack_square.x + 32 
	global.attack_travel_y = nearest_attack_square.y - 32
}
if(dest == d6)
{
	global.attack_travel_x = nearest_attack_square.x + 32 
	global.attack_travel_y = nearest_attack_square.y + 32
}
if(dest == d7)
{
	global.attack_travel_x = nearest_attack_square.x 
	global.attack_travel_y = nearest_attack_square.y - 32
}
if(dest == d8)
{
	global.attack_travel_x = nearest_attack_square.x 
	global.attack_travel_y = nearest_attack_square.y + 32
}