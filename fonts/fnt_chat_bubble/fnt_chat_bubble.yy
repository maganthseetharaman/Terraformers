{
    "id": "e22ef70d-1ef5-4274-9757-b1361b52d0cc",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_chat_bubble",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a2557c40-9b11-4fd8-8bb2-bb708ae5a6be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6b12480e-7e6d-441a-8b43-6c0fa9d8591d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 41,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "be1c81f8-eb67-4f53-a02b-2153d7ab9331",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 35,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d8c73d0d-7863-4a6b-b445-ad8471890f1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 27,
                "y": 62
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "22315fbf-33cf-4fba-b835-711ee1c0f792",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 19,
                "y": 62
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "13ca2be0-f543-4004-bbd4-25d7c605242b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6b158f94-f44c-4c74-bfc3-e23b3e757f6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "58b173dd-e653-4139-9947-7fc155c282ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 117,
                "y": 47
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "497313c4-a74c-4eba-96b1-b17cbb6c153e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 111,
                "y": 47
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "2cb9aea6-c1b6-4efe-9579-7039d60078a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 105,
                "y": 47
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "37042c52-8f4a-41f8-ab72-e70d9358c3db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 45,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "67d92df2-4556-46bb-9d33-f2bc09d9d7c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 97,
                "y": 47
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8a38510d-5156-4750-b4fc-a7edb085a94c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 84,
                "y": 47
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "da79e304-a864-4b17-be95-2236c739abcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 78,
                "y": 47
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7ee0ba7f-9126-47d9-b04f-869989c626a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 74,
                "y": 47
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "9c266490-884f-48e0-a7ff-1fca5e43bf9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 47
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "6d1d554b-0fe8-4fb1-ae53-2bd9cef9a07a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 47
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2278688b-cb92-4690-9892-0194f8895585",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 47
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ad089121-2d33-46c8-aee4-ee81b8363ad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 47
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7b1eb4c6-9089-49bd-bd11-b3720752fe32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 47
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "17f2f1a3-b6f3-49fe-bd95-3c9626898191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 47
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ee15aeb5-d9db-4e66-b888-764561434cc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 89,
                "y": 47
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "441a0a37-e3d0-43fe-932c-bdc37e1d27c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 53,
                "y": 62
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "2ee3005d-7caa-4356-90b0-873b9635d187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 61,
                "y": 62
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ac2b21fb-11d2-4533-8742-594ac392e04e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 69,
                "y": 62
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "10d642a1-bd8e-419c-b6f9-18afabb769d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 101,
                "y": 77
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "bc7a0f12-2feb-4165-af72-b228560984d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 97,
                "y": 77
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6f579d20-e296-4b7b-9399-421a937d3dc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 92,
                "y": 77
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4f630760-f937-459a-854c-1c6a15e0f30d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 85,
                "y": 77
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6d8999a2-5751-4315-85e9-e81ae58909a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 77,
                "y": 77
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "791454b1-db3c-4058-a6be-fb99f1b51c1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 70,
                "y": 77
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "81a3d85c-eb63-4284-b70c-5f7799a3b740",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 64,
                "y": 77
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "3b6865b0-5fc1-4da8-adc4-b821425938d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 56,
                "y": 77
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a70c81f5-39cd-4763-a8bf-8f0ffe123e8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 48,
                "y": 77
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8267e2b4-8e3c-4c4a-bcec-e4834ac92fb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 40,
                "y": 77
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "7e2f7fa3-30f4-485c-b573-2181fa730bb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 32,
                "y": 77
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "62fe56a4-68b3-4b90-8493-32dfe5d5e9b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 24,
                "y": 77
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "f7e12dd7-b596-456b-991b-a1ff68f5297b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 17,
                "y": 77
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "34093601-7cd0-4a41-baf4-179a04057d14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 10,
                "y": 77
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3b668868-4ee6-435b-968e-8592ddd704ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "11c4ee41-4245-47ec-9112-cea5a06f49d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 115,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7201fa2c-12d5-48c8-8d54-aeda26970d31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 107,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "67705c46-7816-4bc9-a60c-25cfb3389493",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 100,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "617ce9bf-985a-47a2-b253-35d20cb46c80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 92,
                "y": 62
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "6ada4ab2-472c-49b4-b5b2-fb8d1ea116c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 85,
                "y": 62
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9a17dcc0-f6e5-4281-b9d9-72671226977e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 77,
                "y": 62
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "bbc31e4e-84d3-4c85-bcac-21e5d1c3c202",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 47
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f6495544-d292-4927-874b-cc3fc471c38d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 47
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c2c91dab-5313-4c92-8e5f-860c6ddc5946",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 47
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e7d22a58-547e-47fb-a051-8abbca9fb923",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 57,
                "y": 17
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "98fe5ffe-596c-48f6-8145-f9833d5a4ccd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 43,
                "y": 17
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b31e3521-1b66-44e6-a920-a3bdf5bf56e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 35,
                "y": 17
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "72cfacc3-7ee0-4235-ad2c-df3394ea4a78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 27,
                "y": 17
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "025f12dc-9c02-4813-869b-0daa4b0425a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 19,
                "y": 17
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "9c28c2ff-8111-4eaf-9264-11e63f1f86a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 10,
                "y": 17
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "72fd6066-3f73-4fd5-b395-49a897d7c361",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 17
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2f61ebc0-2681-4754-961b-0d2cd2b99a11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "dca23d46-6368-416c-a1c1-7b6910e1ec54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "efcaf56f-32b4-42cd-ba1e-e8de961327cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f183c1de-d065-40d4-8fcb-380363d254c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 51,
                "y": 17
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "58edcb34-6865-4fd1-9bae-7bcb9ee72bd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "87691fdd-0dd3-4371-b240-01fab123f76f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1f551047-b60f-4891-9cea-e42c22d19c56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b0638d0a-d629-4585-a25c-6f14a33346c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9fe4c902-c756-4cf9-bc0a-03c206cd7c8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d5421360-03fe-43d1-a9f2-d2dca096e072",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5944f606-133a-4e15-8a90-d615a3ff79d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6d1849d8-fbbb-421a-88d3-38426e70449f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "074ac031-422e-4f45-8315-9d21f0daa8f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4d129a90-1dfa-412e-b103-2ee0dce35766",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b78ead77-437b-434a-b1f6-7eb3387cbd77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "58d915aa-894d-4908-b5c2-6e0ae8e12822",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 17
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "fa2287a9-a6f8-4bfe-886d-d2f1c8767730",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 32
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3972b1a2-a0ce-41fc-8c0b-9b3422082928",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 17
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1488de0e-f21b-4607-a455-2c06d1a376df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 105,
                "y": 32
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ef2eec66-e74f-4d2d-9356-240fd2bac41f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 97,
                "y": 32
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "df45bc53-f0ad-440e-a300-2d4c91bb8317",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 89,
                "y": 32
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d30577a6-db16-406a-b049-79462cfc8a6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 81,
                "y": 32
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ca4a23f0-1ff4-4cf9-9511-6dfef09bff25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 73,
                "y": 32
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "581c9094-f19f-4198-9d43-d1ac26b35d71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 65,
                "y": 32
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4e9ae413-39a6-4579-95f8-befdec16cc84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 57,
                "y": 32
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "977bf2f3-dbb7-4601-8756-9dd8bceb89f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 49,
                "y": 32
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a06b5df2-f5d4-45e0-affd-c2cb8190a52c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 42,
                "y": 32
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "2607d551-3c37-4bc5-a93d-95b6d95880bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 112,
                "y": 32
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "eff343a6-7153-41d5-925b-ac6d5496ce13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 32
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "491199cb-761a-41c7-863b-e35cee44cb92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 32
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "bf906f4f-6fb6-4cb0-844c-85472b8e9a94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 32
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "19d3052a-edf2-4cdd-9b22-2f111dc42e22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "5a2e3aa6-4887-44cd-bd14-cf7eddd24010",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 116,
                "y": 17
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1391615d-3153-4f72-a383-149171e613e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 108,
                "y": 17
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4509b9a3-688a-40cd-972f-276722a4732f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 100,
                "y": 17
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c9cb6dc0-e2e8-4e42-8fdf-4655c6e96fbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 93,
                "y": 17
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e07940d2-084d-49ac-bcfe-f54b7c15a852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 89,
                "y": 17
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d2153fbf-43ce-4f52-9d8c-953880b077bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 82,
                "y": 17
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "30649d2f-b38e-406f-be5e-9f7c212d341c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 109,
                "y": 77
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "b1e29cea-6ac1-417f-b98a-e399d3ca1afa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 13,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 117,
                "y": 77
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}