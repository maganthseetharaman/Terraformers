{
    "id": "e22ef70d-1ef5-4274-9757-b1361b52d0cc",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_chat_bubble",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "10d960e3-0c0b-4ddc-a2a8-36d1ec923f61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "fdd423d2-f467-47e8-882e-4d0fc91938ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 149,
                "y": 44
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e35da6a2-be56-4eaf-ba49-162b0fd93591",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 142,
                "y": 44
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3d753a14-9847-494d-ac81-a362ee4ff162",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 131,
                "y": 44
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "3cd0b652-aa8c-42a4-a6ca-2784a2e33e43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 121,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "559f7fe8-0737-45a4-b411-9cf5c1649010",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 110,
                "y": 44
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "4ce1ff09-cca7-41cb-a5dc-5449e4c6221f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 99,
                "y": 44
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4bb58bc3-09f8-4363-bc00-3dda7205b235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 94,
                "y": 44
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "992ebccf-e1cd-48f7-a561-27e7f83dae33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 87,
                "y": 44
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d5de2d65-ddc0-426b-98d3-7f66f2d3d817",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 80,
                "y": 44
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8aea9249-9629-4b27-92e9-315272c50b79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 154,
                "y": 44
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b4078c0e-8745-4088-b7c6-3e3f7bd58a44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 69,
                "y": 44
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f367c5ba-5a15-44ee-a335-0b9c80f3117d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 53,
                "y": 44
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "fd501a7f-5c8c-4c2a-898b-985d330c6643",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 46,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "6b0f9d08-5234-47be-a2f9-b9214558c0a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 41,
                "y": 44
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "24e8dad1-c779-4c4a-b6f0-066f423ff555",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 31,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9999893d-9514-4e0e-bdea-9a2456a4dc5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 20,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e527c1da-352d-44c4-9cba-781906584fff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 44
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3ccd7ccb-0a26-4ed5-be3f-173ad88fa233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4c25b9cb-db63-49c6-a559-92735758915d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 243,
                "y": 23
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b333db9e-071d-4a02-951c-8792a9d10d58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 232,
                "y": 23
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7f095f21-a96b-4c1f-af21-92a514b17d9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 60,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "855155dd-e7b6-44f4-92c7-3dc819a8a1ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 163,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "15f210dc-f9d4-4a13-99e3-dc5b7dae50ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 173,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "187f79d4-882f-45cc-b6a3-d9422e57cbd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 183,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b27ea369-e10b-4308-adb1-23e5f7d33bad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 129,
                "y": 65
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c9bcbdb8-0b62-4b8c-954a-d55afe273454",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 124,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "dcfd7c41-b449-4e69-9ffe-f48be6a0d333",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 117,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "1db598e0-06b0-4847-bee3-72807a552546",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 108,
                "y": 65
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "907e9360-e26d-40d8-b1e8-f2622f7befd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 99,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fa85f899-e0d7-497d-9bae-d712f4a6b880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 90,
                "y": 65
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a7a67a63-ae9e-4477-b1ca-6b3963f9b4e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 82,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2696d2dd-c1d2-4ead-8834-bd192dc59cc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 71,
                "y": 65
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "6a57341f-a6de-43a0-b8fe-b606c6ff48ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 60,
                "y": 65
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b230dce7-3031-478b-ab0f-55c28223590e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 51,
                "y": 65
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "c11279d7-379e-49f6-a8ff-f7db605527fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 41,
                "y": 65
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "5228cc00-1b4b-4ad5-8f7f-fa77520e3914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 30,
                "y": 65
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "f4256ff6-3833-4d73-8c30-0f7791f1c6ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 21,
                "y": 65
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b534810e-e283-4498-9edc-705cbdbdb1de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 12,
                "y": 65
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b15f9e71-3669-48d4-8c46-79e4de50cf9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "c7ce03db-4841-4217-ae22-5c0b889e2011",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 240,
                "y": 44
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d4b4df86-15e4-475a-b942-e5c71a26c188",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 231,
                "y": 44
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "98d834e4-eba8-44f1-91a4-0a725ed86c68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 223,
                "y": 44
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1c40c4b0-199a-41a7-ad1e-7ad431becbc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 213,
                "y": 44
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0c2d6b9d-51fd-4212-8ed9-5bb0c4028fc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 204,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f5d640ef-e670-418f-a971-c68e28e27405",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 193,
                "y": 44
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "42ea6dde-59a7-4518-8c81-e416cc71954b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 222,
                "y": 23
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0db53d2e-e49d-499d-ae94-39d4e984e3f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 211,
                "y": 23
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "db752f53-2748-4fef-9e6c-7e830077daac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 202,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "873884c7-5cf3-4019-b655-3c8332dc0dae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e5cc05ed-e726-462e-aed1-1b6aa64013ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "5900bfdb-1a2b-417e-9060-624586750ee2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "0ae4ddd4-d0fa-4e4f-98d3-4960c882a4ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6206d4b0-4c42-4382-b2b9-0d538f73429d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7b959f33-3b53-4f4e-9bda-26a816f11417",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6b5fc817-ddf6-4a7d-a695-e685a92baa4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "daa28c31-c095-4837-a186-9309d3583b4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "18a0a47b-3ffc-4b30-bd27-8f8538ca7061",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a3870b92-0f3e-4bab-9afe-fe8ab30344c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "95c26750-1c7c-48e0-90dc-b1c011d01369",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1b335efa-31b3-4f1b-b361-c738764824d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "4d3ba82e-e3b5-4b0f-9e28-740c597ec769",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7df2c2a6-cc9f-4c1a-9f77-d5e7fc13ba44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "3b12ea4b-21cd-463c-8ca0-a5d1d114fb20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "18ca1ed8-e277-42ec-8137-c41728c625c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 6,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9ec68f8d-ed41-4dd5-a139-b04e02f9d331",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d07a25f3-34de-4de3-87b9-b44dfc8232e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5e564430-0295-49b3-963d-9d9703085745",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "9ba505bf-1fc7-4d1b-98c7-d06f0f1645ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "b49065cd-e80c-47db-8d9f-673496063510",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d3414855-398f-4da8-9aa7-819ab536a574",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b2d2aabf-bcb5-417e-a751-747bc789c3c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3b88c151-f535-4bad-9c77-867d9e671f8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 87,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e761e4f4-b9ba-4ce7-a5f3-d2b46415f03f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d9bd3b57-4b5a-48c0-a68c-0861ab6b77c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 185,
                "y": 23
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b1ae26d9-18db-429e-aa5d-69fcfe04f02d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 175,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "935c58e8-4f3f-4b9b-9531-bd3943aebb76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 166,
                "y": 23
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "21ed9c18-b5d3-4b88-a35b-55eccd7b373e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 155,
                "y": 23
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d2a992bc-11ce-404f-87ee-857075be7437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 146,
                "y": 23
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "9b8b6711-651d-49de-8a8a-ad87da604935",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 135,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "48ec8dbb-a8f7-4135-b5e4-357be9a0aab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 126,
                "y": 23
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "61dacb3e-52ae-433d-acc5-9127fd1fc5b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 116,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "3921be1a-38d6-45c1-8a88-da590460011c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 106,
                "y": 23
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a3a5f030-d5fa-4a76-80d7-17a6c072fea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 193,
                "y": 23
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ab46c15a-e8cc-4f72-a585-46b805238a56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 96,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "488aaae0-1721-48fe-8c47-f45aa0f0a8c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 78,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d9247a66-d521-41f3-996d-d4c848d527b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 67,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "92fd00c7-1c70-44c7-8d87-aa0a6542bb91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 56,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4d9046fb-43bf-411b-a8e8-d8ce79555b42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 45,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2d8ded0d-784e-4282-b4a1-5066117c6a8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 34,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9d579c9a-fb95-4b59-9541-1c085ad8f4b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 25,
                "y": 23
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b6918426-1f2d-4147-9b12-5889913b0a68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 16,
                "y": 23
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6b20b7ce-cdf8-4a06-95d8-7bf2036ff86c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 11,
                "y": 23
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ade35184-a3cf-4888-9144-864226c0bfd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c279e605-941d-49f5-8a90-318ea8085384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 139,
                "y": 65
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4b141dde-09c3-4b0d-834c-5452cbd0cc73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 150,
                "y": 65
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}