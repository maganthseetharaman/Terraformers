{
    "id": "c4751285-386d-4828-998e-b7e2f74ba515",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_textbox",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "5f621ca2-eaad-4977-9c90-293750142e30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "51132a52-e0f6-4716-b579-7c4efa4d4747",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 149,
                "y": 44
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5b76de3b-a4ac-42d9-8f45-7e327d9ca203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 142,
                "y": 44
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2076cbf1-fb19-4538-8e69-81f08f8902a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 131,
                "y": 44
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "51e99201-074c-4fda-9309-f27c38b68685",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 121,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "18642b29-179c-488a-81bf-8d011a585909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 110,
                "y": 44
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "67f10c62-3b68-4fec-afb0-45d18a0a4cb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 99,
                "y": 44
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ce00fbf4-d45e-42a6-9e4d-d5aed1e69393",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 94,
                "y": 44
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "8e715f90-f84a-40ba-a106-faadc9ec123a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 87,
                "y": 44
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "0b0aebb6-2582-45e5-bac7-f00a667424dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 80,
                "y": 44
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a844b00e-87c2-49ec-931b-87c1ccf4d61c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 154,
                "y": 44
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "5a954f09-19da-460d-be74-4a77f416f3ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 69,
                "y": 44
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a499d37f-f2cd-4b01-abf5-09283bd149f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 53,
                "y": 44
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8484e855-a0b5-4ada-8339-b68fcd1661d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 46,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7d9a0490-0d37-4065-8147-e2abe20d8f9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 41,
                "y": 44
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "7201751c-fffc-4614-8583-4ca9a1e4b70e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 31,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "03bccaaa-9118-4b80-bce2-3ef7988c3af8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 20,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c235fee3-bc0a-4987-85d2-aa1d34122424",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 44
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "2fc73e64-bf6a-4b77-ae5b-93ff5fe2498c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "04b0bed0-9e56-430c-b829-593d458f794d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 243,
                "y": 23
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2d7308f2-1e49-4da5-80fe-da795437a977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 232,
                "y": 23
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "aa305612-9e74-4766-b05d-1a7c96dcc795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 60,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "31bd371e-9b6b-4457-bbaa-8241d0202ec6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 163,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1c28b351-d0bb-465a-8b8e-dc8e20f6a1b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 173,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "cece6a21-f15f-4684-b27f-276cce75587d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 183,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c8779ce2-acf5-429d-bc28-6aae696dc2d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 129,
                "y": 65
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3c955ca4-0316-47fc-8b90-c80bdf7bdacd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 124,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e7be9c88-d084-4311-8d7e-03479e14e857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 117,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2995f9e2-5dee-46c7-b511-380b70cdb21d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 108,
                "y": 65
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5b358b78-0f11-4975-95b6-3a82a64ae1db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 99,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e4272b31-21e7-4521-9ae6-8ba382cdc3d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 90,
                "y": 65
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "34f79388-73ac-46d7-b62c-502f7abf1671",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 82,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b9520790-6589-4718-b3a0-2988fa8a361e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 71,
                "y": 65
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ed9ab298-9bc0-4fbb-a96c-75e44bfbad79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 60,
                "y": 65
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8267d6a6-173b-410f-83fd-aae799e86fdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 51,
                "y": 65
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8cb27700-94de-4bf5-b859-d76f56ee438d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 41,
                "y": 65
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ba9b6057-36f2-4cc2-9e95-4bbfe79a5544",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 30,
                "y": 65
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "f45386ee-61b2-4a25-8ba4-60d1c0a63458",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 21,
                "y": 65
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ccdc5e61-cf2d-41a5-a5f2-3ab7a293257e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 12,
                "y": 65
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0c0f1b5c-40d5-47aa-92d2-14c44a4cfaf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "0f33183e-3842-43ca-a967-57d832ff90ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 240,
                "y": 44
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4661893e-496b-4883-94c9-a87c57c57203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 231,
                "y": 44
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3112c095-b2c6-4aca-9987-c2c30081ad79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 223,
                "y": 44
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a7e8db1b-52bd-499c-9cda-81b559a1e5e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 213,
                "y": 44
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ef79d16f-442b-4a35-a70b-02a63f2a1871",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 204,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "3d3b7e78-dd9e-4050-aa35-07694af57b55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 193,
                "y": 44
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "59cd6c54-f90b-431d-8648-d50ccf4a1729",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 222,
                "y": 23
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "fdb3b23b-98b1-4e7a-9fdc-604385657ac3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 211,
                "y": 23
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "4bc6bf43-5031-40aa-9552-327e4ace465b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 202,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8b595f8d-498a-4bbe-88e2-76cb58670820",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d8c73ba4-fd8f-4ed2-92e0-201a021ecad3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0ef6b30f-a4c8-4343-ba61-4b4cb349ab98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a461de1b-6496-44ea-9b33-053dd2dbce04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "49f4b5c4-7f28-4bf5-a29a-d0bb8e4ceff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6e97837c-87eb-4b4d-9f4b-dc7dd87e5632",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7baae7a0-ce27-49e9-858c-b107677a1fbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ed86fca5-3db2-417c-b7d2-e8690188f46f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ad46defa-3616-42f7-b8f5-00a46d072403",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b754b167-e898-4d69-bd57-6c42d7782466",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "925150f9-7eba-435c-afb7-3a38009f371e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9f793669-429d-40ee-98da-9f0e6d5a0346",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5fe902f8-fab3-4949-b85c-6bf370152ba9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7035ae04-34aa-4045-9f43-a8a073eee42e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e34d82e4-0b5a-4bc8-83d5-f7f5ad430903",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f0ef3bc3-d49b-4930-9993-cf5a72e7c46d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 6,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6bf81fe8-6158-4dfb-8774-a8d4ebafdd3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "daf17a84-687a-4026-bb29-e13840205aa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "1e96a6de-79b1-4aec-bf56-1accd518f3ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4d260efd-2da0-40fd-96f9-b9bb23b1bc69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "11403ce8-9fd9-46bd-86a8-7ddbf94c7c62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "db444582-d086-4496-bfdf-17efde14a65a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b1c889df-e2ce-4142-bdd8-3e563b63716e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "fcf5d58d-999a-4488-a48b-69dca9751770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 87,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "19de1012-e14b-41ac-a770-549c76f68423",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "666ad84c-25d4-4cff-a7ff-0d7c76272f2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 185,
                "y": 23
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "0866ce48-2115-43e0-8ce5-e3d5cb9d7962",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 175,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "395cbb1f-9349-44b3-9387-c87f5495bfe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 166,
                "y": 23
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "93b3e386-93b4-4a91-90ee-881e61ee2d2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 155,
                "y": 23
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d3e3c29a-799e-423f-b018-ffcfed8c5370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 146,
                "y": 23
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "049bc887-1fd6-4e36-820e-753c9263ecac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 135,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "3e26a538-5b59-4b0e-a2a0-3a283fcf1f1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 126,
                "y": 23
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6cd29d42-4337-4ce0-a9be-ebbe83c51b9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 116,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c16a246c-f0ce-47ef-a0b5-3b8c76d945a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 106,
                "y": 23
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5f4e9048-6dbb-4426-9c02-55ee30ea91d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 193,
                "y": 23
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "db804ac3-3573-4f21-ba0e-d18f05f5cc93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 96,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "0f2f47b1-d70f-430b-b866-e80fe3497d73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 78,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "94688c05-170b-4e86-b9aa-86e83e6a0b4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 67,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3233c3c4-143f-42a4-8891-470f33897fd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 56,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "849bf7bb-53ee-4bb1-9826-4f9d1e23e40f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 45,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a121d051-2b85-4b44-b24c-4b1afe6c35e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 34,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "286c04f0-56d2-46c5-8d43-7beed72862cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 25,
                "y": 23
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "1c7fb59e-bb74-4f23-939e-f6028df21ad6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 16,
                "y": 23
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6473221b-848d-4f6d-968b-b4ec4ef6e7ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 11,
                "y": 23
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "dbac3df1-b2ce-4c2c-9de6-7214267ad3f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "eac77377-cdd7-4d84-ae3f-5a6b94ec8b2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 139,
                "y": 65
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "a5b38b63-35c3-49ac-b7e8-5de3833331a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 150,
                "y": 65
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}