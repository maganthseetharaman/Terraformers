{
    "id": "c4751285-386d-4828-998e-b7e2f74ba515",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_textbox",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "13dae48a-de9f-4473-a370-3a3c0e5748c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "79396c72-76f0-40f0-87f7-40b112da130c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 41,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "6abfc81a-d302-4677-be12-69c662a3dfef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 35,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ec516a93-bc04-4846-ad3a-3ca6ac1fa19a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 27,
                "y": 62
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "bccc8ab8-6cb3-420f-a780-9fb6fcd2126b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 19,
                "y": 62
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "537bcabb-f670-45cc-a44b-985ce1446edc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "62e1f379-b8a2-40c8-9537-a1852411fa06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8791aa7a-8ecc-4a77-aa4c-db36ca8848cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 117,
                "y": 47
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e9bd7941-e88d-40ac-a264-6c0ec5effc00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 111,
                "y": 47
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "26d7a74c-fb52-4188-ad49-f7ebc0bbc952",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 105,
                "y": 47
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ffd5a87a-9db2-4d29-8eea-629c6708245e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 45,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "21893e19-fab3-41f3-a6f7-54033eb4b861",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 97,
                "y": 47
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f8c84a3f-ab56-4ad4-bc0b-6f4a3822ecfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 84,
                "y": 47
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "5070ded1-fcc0-430f-b021-eaab1313f327",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 78,
                "y": 47
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "81134a22-469c-4e89-9122-44f55d74f47f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 74,
                "y": 47
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "682b0370-e8b3-4cd9-b095-8e6577be7f3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 47
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "adf10d09-b613-40bf-ab5a-e36c16f5b778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 47
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ac3b924c-a739-4ed9-a937-f95f54019418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 47
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "874d2788-39ab-4df5-baad-b0df56dc767c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 47
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3cc76c7a-fe90-4d20-9dfa-f4a9a8eec15e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 47
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "520b9b6c-d272-4df0-be8c-2de634e0e906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 47
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "49f47a05-891f-4896-a17e-ad49b3812960",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 89,
                "y": 47
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "978feb32-286d-4411-8697-21adc54350db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 53,
                "y": 62
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "54d9f0f9-368b-4009-b4ab-755ccb5de177",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 61,
                "y": 62
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "912b0fa1-9cba-45a1-9446-032e6e19b308",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 69,
                "y": 62
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "68ec608a-91c1-4a20-b14f-f622a175aef9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 101,
                "y": 77
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ef62855c-cfbc-48c8-9e39-543c298a75f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 97,
                "y": 77
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "dd46de15-db80-4ca0-a8ce-2b2c5e2e7353",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 92,
                "y": 77
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "37da3baf-b6d6-486d-bff0-b87e408ce3c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 85,
                "y": 77
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "fe3b57e2-1acb-49b5-8151-f456c044a45f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 77,
                "y": 77
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "25dd812b-2e42-4ebf-abd6-915e3346407e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 70,
                "y": 77
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2d860209-5b38-4ba2-9051-bb991eda15b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 64,
                "y": 77
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "3f8a22da-318d-4b4f-9b82-ca1d7db5baad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 56,
                "y": 77
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "5caed9d0-5091-4932-a105-a6d81a97f875",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 48,
                "y": 77
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "88a96b9a-6d87-43dc-86b3-68d9af853a10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 40,
                "y": 77
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "40362a85-56b6-451b-9034-36fb71819772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 32,
                "y": 77
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c9016900-af8a-4f91-997f-8e39aca8ed51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 24,
                "y": 77
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "441058b0-29d6-4e7d-9180-eeb0c48a471f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 17,
                "y": 77
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a31ae43e-7645-4ee1-92fb-d01d6196d905",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 10,
                "y": 77
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "767148e6-2274-4676-b604-7abaec59cce0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "ef2180a7-cd1a-416c-8c64-68dbaa12c37a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 115,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f1e36bd8-86b0-4d51-8665-5369fc8e1c1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 107,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "be4aa552-a5f0-4dbf-ab6c-b9a6e01571c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 100,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6dcd8bb7-b6e2-4e79-b86d-8d24dbf21d0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 92,
                "y": 62
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "839b3432-95ab-47c6-ab1b-b21eee7f7380",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 85,
                "y": 62
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6acd3773-2366-45a3-87e2-2f456f4f71cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 77,
                "y": 62
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8bf7ebb7-b109-43bc-a03f-1728f3a82488",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 47
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e24c3ef3-9c30-46ed-85d4-c404377a4638",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 47
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5298791c-c99b-4b12-862b-b395cc3fddb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 47
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b59f1cfd-e592-44fa-b5ba-14539d874515",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 57,
                "y": 17
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "61cb6f01-251c-4bc1-8c4c-6c441ab78efb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 43,
                "y": 17
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "9d90ef68-5b9b-4095-8c26-0bbc385548c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 35,
                "y": 17
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "40078a56-8833-431b-8644-611b08d06421",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 27,
                "y": 17
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c4636433-6ad5-4aee-bd1f-6e24ad62e1a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 19,
                "y": 17
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "9150af6b-afe9-4baa-abb1-b23ed0bd5220",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 10,
                "y": 17
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "46d7b15e-445e-4f01-b0c1-8a833034b8a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 17
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "3ba7f6c6-83fc-4d5a-a1ba-84514f9e3c9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "91744afa-adea-4d11-a891-b2aad4a34c93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "5824e92c-8e44-4c00-9ad7-cd9c0f620dcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "de810a91-1029-46d6-bf71-522d799038b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 51,
                "y": 17
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "25141dc2-e457-401f-85a3-36bbb2f384a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8e7989ea-6c65-45b9-b93f-91726ab533ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "907cee4d-e9dd-4fb3-9194-2f8c566acd2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b54d8f31-980c-4ef8-b612-d8646144ecc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e06f5bad-33e6-43f9-89c2-8d9efefa4aac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "5e1e7b4f-ca81-4192-8f24-e3ba231425e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d8224134-874c-4d31-97c9-5e32929c9d56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "58a04546-e543-4880-abc6-b4342018af13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "450e6de5-876c-415a-b1a3-f697c503d5f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9992b66c-2835-4e33-82fd-ac2d51e40c2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "807f0194-f308-4e8c-94f6-90bef1f6c393",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "5bcc8ad9-aa31-46da-887e-817e2bbab427",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 17
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "2f322497-7320-4f06-a3fb-e92617085381",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 32
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "6df4d2f7-5609-4beb-a892-bc51f96c0199",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 17
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "90a0c718-9f64-4436-b515-a4bbf882622b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 105,
                "y": 32
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "53629c4f-3ed7-4bba-95db-5172f915b6f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 97,
                "y": 32
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "0f50eb2d-b598-40fc-9243-62835026d670",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 89,
                "y": 32
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "cbbc5792-b983-4476-96b6-2550f6cf365c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 81,
                "y": 32
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "29b5d475-96a1-4898-9f32-ee76fbcffaaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 73,
                "y": 32
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "08636b21-a3ab-4c8d-9695-ef9f2ca736f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 65,
                "y": 32
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e9eeb77e-4a5f-4094-8792-f98bd0bab545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 57,
                "y": 32
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6d310c4f-ae41-4fcc-ac7b-18b4286cd72c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 49,
                "y": 32
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "13f8e8ad-28d0-4f07-8913-602118adc603",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 42,
                "y": 32
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8dddd863-e87b-4157-941b-a1490fc50486",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 112,
                "y": 32
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "dee520fd-502e-4901-a50c-2e105aff19bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 32
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f672d712-9849-4629-acdc-73714df70798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 32
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e3823112-f01e-4347-aab1-bc960456ae95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 32
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f79f2f70-dba0-40ea-acdb-2179222c379d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "df734480-d3b6-4cd8-b464-149fc7d7a123",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 116,
                "y": 17
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "01d04adc-05a1-437e-bb01-eb4f9550169b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 108,
                "y": 17
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0a2b4dd4-a6a7-443e-9a3f-29bc0c06b929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 100,
                "y": 17
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "9249f589-b52a-4963-baba-fa8e2b6c48d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 93,
                "y": 17
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "63ee987d-c68e-4c74-b544-09ffaddcc298",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 89,
                "y": 17
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "34e8b6dc-6a5f-431d-9dfd-453c2448b871",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 82,
                "y": 17
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6ca388bb-28b8-4aa3-a87d-e85ce5593da7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 109,
                "y": 77
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "0fc26e83-1eef-42df-af32-597514806485",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 13,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 117,
                "y": 77
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}