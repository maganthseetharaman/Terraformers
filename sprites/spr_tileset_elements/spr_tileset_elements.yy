{
    "id": "ebe23187-5b0d-41f5-8514-c74973af13be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_elements",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 383,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a58d699b-7579-44a4-9efd-87d1b8d24319",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebe23187-5b0d-41f5-8514-c74973af13be",
            "compositeImage": {
                "id": "bd04ab4b-e957-42bc-9b67-bf63fc38b2f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a58d699b-7579-44a4-9efd-87d1b8d24319",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1538400b-9ad7-4b91-9c06-aed8930a08fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a58d699b-7579-44a4-9efd-87d1b8d24319",
                    "LayerId": "45eb84a6-8956-4254-98ac-125f38e4edae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 384,
    "layers": [
        {
            "id": "45eb84a6-8956-4254-98ac-125f38e4edae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebe23187-5b0d-41f5-8514-c74973af13be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}