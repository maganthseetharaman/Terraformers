{
    "id": "f60b901b-783d-45da-b639-7204b45330c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_team2_square",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b67f1e3b-306c-48bd-ac1f-f1352e5083e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f60b901b-783d-45da-b639-7204b45330c5",
            "compositeImage": {
                "id": "8bc09d22-757e-46be-98c2-19f7a1e36b55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b67f1e3b-306c-48bd-ac1f-f1352e5083e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd868032-3ef2-49bd-99ee-967d1afc3a5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b67f1e3b-306c-48bd-ac1f-f1352e5083e9",
                    "LayerId": "247fe1c7-fb7e-454b-a7e2-3b060abfa1f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "247fe1c7-fb7e-454b-a7e2-3b060abfa1f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f60b901b-783d-45da-b639-7204b45330c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}