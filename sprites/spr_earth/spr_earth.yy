{
    "id": "df8adc3f-baa3-40f0-8c9f-ab0436fe3177",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_earth",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 102,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c46d330f-4571-46dd-931b-06372f466643",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df8adc3f-baa3-40f0-8c9f-ab0436fe3177",
            "compositeImage": {
                "id": "1f4fb2d6-9b56-4f01-b8e4-64092b7c527f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c46d330f-4571-46dd-931b-06372f466643",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72c996a5-a9f8-4f0f-88a2-0c2bbdd67fb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c46d330f-4571-46dd-931b-06372f466643",
                    "LayerId": "8f84d6a6-c2af-491f-973e-c16b0b72b48d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8f84d6a6-c2af-491f-973e-c16b0b72b48d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df8adc3f-baa3-40f0-8c9f-ab0436fe3177",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 103,
    "xorig": 0,
    "yorig": 0
}