{
    "id": "dccb3e1b-ec7c-442e-9582-0b0f20d74ffc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_warp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "049caf46-a5ad-48c6-a989-a2ebe4397846",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dccb3e1b-ec7c-442e-9582-0b0f20d74ffc",
            "compositeImage": {
                "id": "7afb30c6-83f9-46c1-89c0-6d1f5e8f497d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "049caf46-a5ad-48c6-a989-a2ebe4397846",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "466e5d93-5b73-41bb-88ff-f05b85fee269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "049caf46-a5ad-48c6-a989-a2ebe4397846",
                    "LayerId": "f8eb0c92-f342-4d7b-9302-8be7eeb75345"
                }
            ]
        },
        {
            "id": "cc7adbbf-f13f-4475-8ffc-588659d04ebf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dccb3e1b-ec7c-442e-9582-0b0f20d74ffc",
            "compositeImage": {
                "id": "dd7b909d-681c-4ed3-bd7a-d0098081d9aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc7adbbf-f13f-4475-8ffc-588659d04ebf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a627908a-0aea-413d-ac41-4789ae57da90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc7adbbf-f13f-4475-8ffc-588659d04ebf",
                    "LayerId": "f8eb0c92-f342-4d7b-9302-8be7eeb75345"
                }
            ]
        },
        {
            "id": "204e431c-4e5c-4941-b9ed-a0a39e97fb52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dccb3e1b-ec7c-442e-9582-0b0f20d74ffc",
            "compositeImage": {
                "id": "dd38a0c2-0ead-4ee5-b629-1f0166422b67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "204e431c-4e5c-4941-b9ed-a0a39e97fb52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f45945c-fb35-42c5-bc96-d04b19de39df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "204e431c-4e5c-4941-b9ed-a0a39e97fb52",
                    "LayerId": "f8eb0c92-f342-4d7b-9302-8be7eeb75345"
                }
            ]
        },
        {
            "id": "5b02c686-35c4-414a-b4aa-1d88ef39ed61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dccb3e1b-ec7c-442e-9582-0b0f20d74ffc",
            "compositeImage": {
                "id": "5e89d858-68bf-4aff-bf78-b2c4d361cfc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b02c686-35c4-414a-b4aa-1d88ef39ed61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a938db0e-5122-4808-8771-79de6a887c75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b02c686-35c4-414a-b4aa-1d88ef39ed61",
                    "LayerId": "f8eb0c92-f342-4d7b-9302-8be7eeb75345"
                }
            ]
        },
        {
            "id": "09c8cb45-216c-46b6-8449-392c7d421810",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dccb3e1b-ec7c-442e-9582-0b0f20d74ffc",
            "compositeImage": {
                "id": "63860c63-a091-4d8a-8394-fefb9d87f55f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09c8cb45-216c-46b6-8449-392c7d421810",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "879dbd2b-0f28-4250-950a-801d816586e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09c8cb45-216c-46b6-8449-392c7d421810",
                    "LayerId": "f8eb0c92-f342-4d7b-9302-8be7eeb75345"
                }
            ]
        },
        {
            "id": "8dd33370-ef7b-4150-9a37-23e13d88d39e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dccb3e1b-ec7c-442e-9582-0b0f20d74ffc",
            "compositeImage": {
                "id": "3779b0db-e07c-4c47-99d9-cc0fa77b6e90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dd33370-ef7b-4150-9a37-23e13d88d39e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc8b51df-9686-4fb0-ae0d-e7a9ac07fec2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dd33370-ef7b-4150-9a37-23e13d88d39e",
                    "LayerId": "f8eb0c92-f342-4d7b-9302-8be7eeb75345"
                }
            ]
        },
        {
            "id": "60d9d95f-4fc5-4548-982e-16ad6dca9d2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dccb3e1b-ec7c-442e-9582-0b0f20d74ffc",
            "compositeImage": {
                "id": "4751ca26-457c-471f-b82b-ea93b259db8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60d9d95f-4fc5-4548-982e-16ad6dca9d2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2362ee4a-24eb-4a03-9623-6d1101965b1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60d9d95f-4fc5-4548-982e-16ad6dca9d2b",
                    "LayerId": "f8eb0c92-f342-4d7b-9302-8be7eeb75345"
                }
            ]
        },
        {
            "id": "379c1432-acb3-49b3-b966-008635b64590",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dccb3e1b-ec7c-442e-9582-0b0f20d74ffc",
            "compositeImage": {
                "id": "f62a5663-8e43-44d2-8311-1eacbe402aa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "379c1432-acb3-49b3-b966-008635b64590",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10c211df-9967-49f5-804e-d15613c951b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "379c1432-acb3-49b3-b966-008635b64590",
                    "LayerId": "f8eb0c92-f342-4d7b-9302-8be7eeb75345"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f8eb0c92-f342-4d7b-9302-8be7eeb75345",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dccb3e1b-ec7c-442e-9582-0b0f20d74ffc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}