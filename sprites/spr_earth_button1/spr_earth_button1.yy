{
    "id": "293a458a-c3b1-4f62-9f67-62510ed21023",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_earth_button1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f2ac73c-2344-4a23-bca0-c0e9fcf1d2e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "293a458a-c3b1-4f62-9f67-62510ed21023",
            "compositeImage": {
                "id": "f810aff6-089b-4e8a-9d97-a473c17f19b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f2ac73c-2344-4a23-bca0-c0e9fcf1d2e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "935f5f5d-33d5-4cf9-9468-471fd393da30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f2ac73c-2344-4a23-bca0-c0e9fcf1d2e6",
                    "LayerId": "638b2073-fce6-486c-b805-477d950d3f45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "638b2073-fce6-486c-b805-477d950d3f45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "293a458a-c3b1-4f62-9f67-62510ed21023",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}