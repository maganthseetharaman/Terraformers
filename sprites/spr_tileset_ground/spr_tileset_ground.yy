{
    "id": "ab21fb4b-c3a0-4b71-bed5-410625e19f0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_ground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 383,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46c4fc4e-44c0-46ce-9a75-6ce3e7dc70d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab21fb4b-c3a0-4b71-bed5-410625e19f0c",
            "compositeImage": {
                "id": "8db94562-6596-4399-a5f5-9857ddc28e64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46c4fc4e-44c0-46ce-9a75-6ce3e7dc70d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9387894a-6d2d-43ed-9e58-9b92e45d323b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46c4fc4e-44c0-46ce-9a75-6ce3e7dc70d8",
                    "LayerId": "6210a422-55f9-4398-955e-ee607f93bec6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 384,
    "layers": [
        {
            "id": "6210a422-55f9-4398-955e-ee607f93bec6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab21fb4b-c3a0-4b71-bed5-410625e19f0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}