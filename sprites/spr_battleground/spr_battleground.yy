{
    "id": "25d1f495-3549-4ac8-97c0-561a2e0cbb3d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battleground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 699,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c386db81-1712-452f-b02f-b41abf938fe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25d1f495-3549-4ac8-97c0-561a2e0cbb3d",
            "compositeImage": {
                "id": "8cd49cfe-bf62-4e4f-9ca5-451c14e0853e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c386db81-1712-452f-b02f-b41abf938fe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9e12e5e-0e7a-4b05-b6f0-998589f8c0bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c386db81-1712-452f-b02f-b41abf938fe1",
                    "LayerId": "46ed0e0e-9bb0-48e1-bb23-dcb7a879f334"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "46ed0e0e-9bb0-48e1-bb23-dcb7a879f334",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25d1f495-3549-4ac8-97c0-561a2e0cbb3d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 700,
    "xorig": 0,
    "yorig": 0
}