{
    "id": "7122eac5-b310-44ca-92ca-93b1402418f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_air",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2da015b-88ee-44b7-898f-d6a447affb90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7122eac5-b310-44ca-92ca-93b1402418f5",
            "compositeImage": {
                "id": "5b9b0752-c4d5-4e6f-9cf9-c5dc9dcd31ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2da015b-88ee-44b7-898f-d6a447affb90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e08951b8-096a-416e-ab72-54f07450aa64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2da015b-88ee-44b7-898f-d6a447affb90",
                    "LayerId": "ab88f7ca-6f20-45e0-a90d-e5eb1705c144"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ab88f7ca-6f20-45e0-a90d-e5eb1705c144",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7122eac5-b310-44ca-92ca-93b1402418f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}