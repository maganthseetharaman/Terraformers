{
    "id": "9fef49cd-095d-4f86-b95f-42f48c243229",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 84,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f76d3130-b1ce-4a3b-ae29-6500b552f8f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fef49cd-095d-4f86-b95f-42f48c243229",
            "compositeImage": {
                "id": "8c1ee2ca-b703-4500-b7bf-43547d2fa14a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f76d3130-b1ce-4a3b-ae29-6500b552f8f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17a7e300-efc8-4e5e-9d44-7389a327f425",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f76d3130-b1ce-4a3b-ae29-6500b552f8f4",
                    "LayerId": "ce055eda-d5c1-4bda-ab7b-f1cbb66287ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ce055eda-d5c1-4bda-ab7b-f1cbb66287ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9fef49cd-095d-4f86-b95f-42f48c243229",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 85,
    "xorig": 0,
    "yorig": 0
}