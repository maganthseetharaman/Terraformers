{
    "id": "0fe96f4d-2e6f-4c78-84db-5c7e09ffdd19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_flora",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78f0b68d-a989-496f-9165-06059f68e582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fe96f4d-2e6f-4c78-84db-5c7e09ffdd19",
            "compositeImage": {
                "id": "1b40b24a-f9d1-4031-9864-a5a6826157f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78f0b68d-a989-496f-9165-06059f68e582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b56bbf8b-f9b0-4fcf-b650-8bc1c5555dad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78f0b68d-a989-496f-9165-06059f68e582",
                    "LayerId": "e55b8cab-a862-4c33-80df-16ecdb3c8fcc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "e55b8cab-a862-4c33-80df-16ecdb3c8fcc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0fe96f4d-2e6f-4c78-84db-5c7e09ffdd19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 498,
    "yorig": 501
}