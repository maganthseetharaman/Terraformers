{
    "id": "7b50f08f-58d7-4235-80c6-5fd51474ca89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_test_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b14cdb6d-e487-4256-873c-48e51a5b59e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b50f08f-58d7-4235-80c6-5fd51474ca89",
            "compositeImage": {
                "id": "7ca075ca-6197-4e76-a883-cfb6eabb9937",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b14cdb6d-e487-4256-873c-48e51a5b59e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aadc918d-8c88-4082-bc96-c685058acef6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b14cdb6d-e487-4256-873c-48e51a5b59e6",
                    "LayerId": "2c3ead89-2d37-4fc8-93db-b8ff78721676"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2c3ead89-2d37-4fc8-93db-b8ff78721676",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b50f08f-58d7-4235-80c6-5fd51474ca89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}