{
    "id": "46fc482d-21fd-4512-943b-78f0e1d85ea6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_square",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f376fd88-c530-48d5-aa62-f13807532b6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46fc482d-21fd-4512-943b-78f0e1d85ea6",
            "compositeImage": {
                "id": "e4f3ca05-b6c2-4111-a48b-3614683b49f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f376fd88-c530-48d5-aa62-f13807532b6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffef6510-d3bb-423c-8e7d-85de6cc7ae68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f376fd88-c530-48d5-aa62-f13807532b6f",
                    "LayerId": "ed30e7f6-8355-433d-95f8-ae1771359957"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ed30e7f6-8355-433d-95f8-ae1771359957",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46fc482d-21fd-4512-943b-78f0e1d85ea6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}