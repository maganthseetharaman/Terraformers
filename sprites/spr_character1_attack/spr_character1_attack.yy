{
    "id": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_character1_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 33,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26565262-b5ac-4a26-ad6e-3958a5d44b5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
            "compositeImage": {
                "id": "a534ea97-aa14-46d0-8589-c5408344bba0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26565262-b5ac-4a26-ad6e-3958a5d44b5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d12f5ddd-2370-4fe0-9709-c74f30a10342",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26565262-b5ac-4a26-ad6e-3958a5d44b5a",
                    "LayerId": "148c4527-c4ab-4163-a82a-d42f5923a5a8"
                }
            ]
        },
        {
            "id": "1e46c4a9-589c-4d57-8142-6af915e74069",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
            "compositeImage": {
                "id": "cf565a0a-d108-4baa-82a0-8c49a1261136",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e46c4a9-589c-4d57-8142-6af915e74069",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfefd83d-345b-4331-af1d-878447a46ff5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e46c4a9-589c-4d57-8142-6af915e74069",
                    "LayerId": "148c4527-c4ab-4163-a82a-d42f5923a5a8"
                }
            ]
        },
        {
            "id": "bdecf1c0-c40d-4692-b589-44221b61a67d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
            "compositeImage": {
                "id": "930821ad-5bc6-42e1-827c-7f2f1132a708",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdecf1c0-c40d-4692-b589-44221b61a67d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daecdad1-28c5-4918-835e-db949970c11a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdecf1c0-c40d-4692-b589-44221b61a67d",
                    "LayerId": "148c4527-c4ab-4163-a82a-d42f5923a5a8"
                }
            ]
        },
        {
            "id": "7c0ec3a4-3137-49fa-831f-26fe8e8ed125",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
            "compositeImage": {
                "id": "33e02bc9-712a-466b-9509-78780b46f710",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c0ec3a4-3137-49fa-831f-26fe8e8ed125",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eff9ec38-a2d6-4c8b-91fc-70b880a92bb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c0ec3a4-3137-49fa-831f-26fe8e8ed125",
                    "LayerId": "148c4527-c4ab-4163-a82a-d42f5923a5a8"
                }
            ]
        },
        {
            "id": "a14ac46f-41f5-4eb2-a595-3c5fe1ea8f3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
            "compositeImage": {
                "id": "78c57e54-2d2d-4d9d-822f-593b69cb4729",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a14ac46f-41f5-4eb2-a595-3c5fe1ea8f3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b36ef546-08b4-47d4-b57d-6300cc7307d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a14ac46f-41f5-4eb2-a595-3c5fe1ea8f3e",
                    "LayerId": "148c4527-c4ab-4163-a82a-d42f5923a5a8"
                }
            ]
        },
        {
            "id": "32a0d1e3-359c-4b65-8fab-196b98269e43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
            "compositeImage": {
                "id": "67bff72f-8d77-41dc-98a5-ca62259d25cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32a0d1e3-359c-4b65-8fab-196b98269e43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f099966-9793-45d6-8ad4-8cf3138ed3be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32a0d1e3-359c-4b65-8fab-196b98269e43",
                    "LayerId": "148c4527-c4ab-4163-a82a-d42f5923a5a8"
                }
            ]
        },
        {
            "id": "efb83be6-ea4e-4fbf-b8d6-e5a133bf258b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
            "compositeImage": {
                "id": "1c9643e9-db35-4e40-b8e0-c3bb1dec047f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efb83be6-ea4e-4fbf-b8d6-e5a133bf258b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20a686a4-1d6b-4548-8318-780c29624c85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efb83be6-ea4e-4fbf-b8d6-e5a133bf258b",
                    "LayerId": "148c4527-c4ab-4163-a82a-d42f5923a5a8"
                }
            ]
        },
        {
            "id": "857f6372-956a-4058-b263-cd84f2aa9104",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
            "compositeImage": {
                "id": "eab98b9a-989d-4754-a1cf-0f4911599d69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "857f6372-956a-4058-b263-cd84f2aa9104",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d11b49df-6d27-41b0-b472-69d2b91270e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "857f6372-956a-4058-b263-cd84f2aa9104",
                    "LayerId": "148c4527-c4ab-4163-a82a-d42f5923a5a8"
                }
            ]
        },
        {
            "id": "3eeb76ad-de8d-4114-b206-3e63b0e365a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
            "compositeImage": {
                "id": "a407f52a-b7d4-444d-898b-3dd2a8281dc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3eeb76ad-de8d-4114-b206-3e63b0e365a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8257546b-5dd3-417c-a594-3ee83a89f8c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3eeb76ad-de8d-4114-b206-3e63b0e365a3",
                    "LayerId": "148c4527-c4ab-4163-a82a-d42f5923a5a8"
                }
            ]
        },
        {
            "id": "93713ca0-7431-4175-bb80-821b01726048",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
            "compositeImage": {
                "id": "e568a9a0-3669-4a39-9089-13954b5ab855",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93713ca0-7431-4175-bb80-821b01726048",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ca6829f-0471-4d9b-bfbc-1e36830b7052",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93713ca0-7431-4175-bb80-821b01726048",
                    "LayerId": "148c4527-c4ab-4163-a82a-d42f5923a5a8"
                }
            ]
        },
        {
            "id": "0266f3ab-247b-4ff2-9e47-8cb933e1cbfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
            "compositeImage": {
                "id": "c5b88c90-1357-454e-a4aa-f23fdf4f7775",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0266f3ab-247b-4ff2-9e47-8cb933e1cbfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ded72379-043f-4845-a419-df7a3ddc5dce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0266f3ab-247b-4ff2-9e47-8cb933e1cbfd",
                    "LayerId": "148c4527-c4ab-4163-a82a-d42f5923a5a8"
                }
            ]
        },
        {
            "id": "8ac1537a-b41c-4ae0-b0e4-29475bec1279",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
            "compositeImage": {
                "id": "4f14810c-63da-43ea-a081-f4f3b72139e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ac1537a-b41c-4ae0-b0e4-29475bec1279",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dffc1c0-abec-4510-a2ac-c9b013a622ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ac1537a-b41c-4ae0-b0e4-29475bec1279",
                    "LayerId": "148c4527-c4ab-4163-a82a-d42f5923a5a8"
                }
            ]
        },
        {
            "id": "0f76c187-22b2-454f-894b-854dc3f8abc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
            "compositeImage": {
                "id": "2c79146a-6afb-4ed5-b04c-0efb2e79da3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f76c187-22b2-454f-894b-854dc3f8abc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a57dac65-0bf6-4831-a248-3512b91e65a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f76c187-22b2-454f-894b-854dc3f8abc4",
                    "LayerId": "148c4527-c4ab-4163-a82a-d42f5923a5a8"
                }
            ]
        },
        {
            "id": "8e2224cf-55b2-4b26-a01a-baa48edb2ffb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
            "compositeImage": {
                "id": "c3ef7fde-6839-4c41-8bce-fceca9ae8b4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e2224cf-55b2-4b26-a01a-baa48edb2ffb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77e10fc4-eace-4e4c-a57b-706d7f4e9364",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e2224cf-55b2-4b26-a01a-baa48edb2ffb",
                    "LayerId": "148c4527-c4ab-4163-a82a-d42f5923a5a8"
                }
            ]
        },
        {
            "id": "8570c923-f797-43fc-9bac-362e3b9912c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
            "compositeImage": {
                "id": "79b0f755-a888-45c5-8c3a-822457aab937",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8570c923-f797-43fc-9bac-362e3b9912c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfd8d050-2070-42fd-9b88-fdad588d64b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8570c923-f797-43fc-9bac-362e3b9912c0",
                    "LayerId": "148c4527-c4ab-4163-a82a-d42f5923a5a8"
                }
            ]
        },
        {
            "id": "fee8021b-ab21-4da3-864d-bfe73cf51876",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
            "compositeImage": {
                "id": "f97060da-eaa6-42df-b691-0cfed438dd54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fee8021b-ab21-4da3-864d-bfe73cf51876",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "711d4447-7310-41eb-9081-1b4986ffd8ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fee8021b-ab21-4da3-864d-bfe73cf51876",
                    "LayerId": "148c4527-c4ab-4163-a82a-d42f5923a5a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "148c4527-c4ab-4163-a82a-d42f5923a5a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f5f08e1-e901-4bb5-ac2a-ab515f485684",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 0,
    "yorig": 0
}