{
    "id": "8ca10948-ffb5-4dfd-83fa-e9e76b7fe426",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf25dd34-1b51-4f02-ac87-9cc81bdb9325",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ca10948-ffb5-4dfd-83fa-e9e76b7fe426",
            "compositeImage": {
                "id": "052234e3-e52f-4587-80a8-c5f4c373b268",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf25dd34-1b51-4f02-ac87-9cc81bdb9325",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd58106d-2658-4887-a9fd-17d463d49a14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf25dd34-1b51-4f02-ac87-9cc81bdb9325",
                    "LayerId": "4a240371-8bb7-45cb-af73-51e6f6cc3434"
                }
            ]
        },
        {
            "id": "0cc37a82-8b9b-49e7-8106-4b9a8b366b7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ca10948-ffb5-4dfd-83fa-e9e76b7fe426",
            "compositeImage": {
                "id": "69c793a6-129c-429c-9c53-16bc250928f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cc37a82-8b9b-49e7-8106-4b9a8b366b7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c045f0d7-4d99-4a02-8bd6-5a9c13ef4399",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cc37a82-8b9b-49e7-8106-4b9a8b366b7e",
                    "LayerId": "4a240371-8bb7-45cb-af73-51e6f6cc3434"
                }
            ]
        },
        {
            "id": "5bdcdb80-489f-4dd5-8b24-3db4e1ab830b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ca10948-ffb5-4dfd-83fa-e9e76b7fe426",
            "compositeImage": {
                "id": "c4eea23c-5863-4489-93f6-88828e5b852c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bdcdb80-489f-4dd5-8b24-3db4e1ab830b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36f3857d-3afe-4f7a-a486-4551367efdea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bdcdb80-489f-4dd5-8b24-3db4e1ab830b",
                    "LayerId": "4a240371-8bb7-45cb-af73-51e6f6cc3434"
                }
            ]
        },
        {
            "id": "45125f60-98b8-464f-a3bc-035958c1f921",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ca10948-ffb5-4dfd-83fa-e9e76b7fe426",
            "compositeImage": {
                "id": "e101e855-e2dd-473b-84f3-b7e073acb7e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45125f60-98b8-464f-a3bc-035958c1f921",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a45e803-7fe3-494b-907b-960efa385486",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45125f60-98b8-464f-a3bc-035958c1f921",
                    "LayerId": "4a240371-8bb7-45cb-af73-51e6f6cc3434"
                }
            ]
        },
        {
            "id": "9a181c80-0705-47d4-a65c-3c1a69b3a4c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ca10948-ffb5-4dfd-83fa-e9e76b7fe426",
            "compositeImage": {
                "id": "229db445-754b-405e-b817-c82d38a84444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a181c80-0705-47d4-a65c-3c1a69b3a4c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb6c41d7-c6cc-495d-bf71-8ed7a05aa1d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a181c80-0705-47d4-a65c-3c1a69b3a4c2",
                    "LayerId": "4a240371-8bb7-45cb-af73-51e6f6cc3434"
                }
            ]
        },
        {
            "id": "6eb97e22-42b4-4aaf-938b-456637354db5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ca10948-ffb5-4dfd-83fa-e9e76b7fe426",
            "compositeImage": {
                "id": "6649e812-f145-4fb2-ae77-ce2f4e6f3a1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eb97e22-42b4-4aaf-938b-456637354db5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ebcc1af-1da9-46c6-97e5-7dbb5cefcb28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eb97e22-42b4-4aaf-938b-456637354db5",
                    "LayerId": "4a240371-8bb7-45cb-af73-51e6f6cc3434"
                }
            ]
        },
        {
            "id": "d93395c6-1b67-4d96-985b-09d21eaa9059",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ca10948-ffb5-4dfd-83fa-e9e76b7fe426",
            "compositeImage": {
                "id": "1991df41-dc57-4012-92da-e79e1f500e73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d93395c6-1b67-4d96-985b-09d21eaa9059",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8255339d-0c7c-45f9-a670-dcccd54efcfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d93395c6-1b67-4d96-985b-09d21eaa9059",
                    "LayerId": "4a240371-8bb7-45cb-af73-51e6f6cc3434"
                }
            ]
        },
        {
            "id": "fa58bdf0-3d5b-4e3e-815a-ec3dec7d29a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ca10948-ffb5-4dfd-83fa-e9e76b7fe426",
            "compositeImage": {
                "id": "fe591ba4-d693-4149-8a9c-9b98a24c104e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa58bdf0-3d5b-4e3e-815a-ec3dec7d29a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5207f46-5dc2-48fc-992f-3763861eb4f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa58bdf0-3d5b-4e3e-815a-ec3dec7d29a0",
                    "LayerId": "4a240371-8bb7-45cb-af73-51e6f6cc3434"
                }
            ]
        },
        {
            "id": "dae49d6d-e269-41ea-b93b-fb976ccf11fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ca10948-ffb5-4dfd-83fa-e9e76b7fe426",
            "compositeImage": {
                "id": "cb37510d-13ef-4ce8-931e-ec1308560d7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dae49d6d-e269-41ea-b93b-fb976ccf11fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7c19367-b8d9-463f-848a-35b032db4e1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dae49d6d-e269-41ea-b93b-fb976ccf11fb",
                    "LayerId": "4a240371-8bb7-45cb-af73-51e6f6cc3434"
                }
            ]
        },
        {
            "id": "aaf98c61-c6b1-4283-8a5f-0e081521ca2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ca10948-ffb5-4dfd-83fa-e9e76b7fe426",
            "compositeImage": {
                "id": "9adbf315-25b4-4cba-a6bd-982f179076d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaf98c61-c6b1-4283-8a5f-0e081521ca2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f9ff1ce-41dc-4cb6-84d2-ad1d664dd805",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaf98c61-c6b1-4283-8a5f-0e081521ca2c",
                    "LayerId": "4a240371-8bb7-45cb-af73-51e6f6cc3434"
                }
            ]
        },
        {
            "id": "66ecb1f7-64b9-4491-a2e6-a48b91028d1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ca10948-ffb5-4dfd-83fa-e9e76b7fe426",
            "compositeImage": {
                "id": "ba3ad992-238c-4bcb-b6a4-110beb8f41ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66ecb1f7-64b9-4491-a2e6-a48b91028d1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4612a935-b177-423b-9cb7-6c32cab7aaec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66ecb1f7-64b9-4491-a2e6-a48b91028d1c",
                    "LayerId": "4a240371-8bb7-45cb-af73-51e6f6cc3434"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4a240371-8bb7-45cb-af73-51e6f6cc3434",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ca10948-ffb5-4dfd-83fa-e9e76b7fe426",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}