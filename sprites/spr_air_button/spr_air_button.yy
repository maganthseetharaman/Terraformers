{
    "id": "be23e64e-30bf-4e23-9630-aee6b2e4bfda",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_air_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38bd1908-e797-4f69-ac2d-a95e04c4c0a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be23e64e-30bf-4e23-9630-aee6b2e4bfda",
            "compositeImage": {
                "id": "05ee16a5-4155-4a0a-b02f-569c7b2821d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38bd1908-e797-4f69-ac2d-a95e04c4c0a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a2c2aea-16e7-48b0-8ee8-32f2bede1f90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38bd1908-e797-4f69-ac2d-a95e04c4c0a1",
                    "LayerId": "09c816eb-dbab-4c10-8821-13c1869d2c7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "09c816eb-dbab-4c10-8821-13c1869d2c7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be23e64e-30bf-4e23-9630-aee6b2e4bfda",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}