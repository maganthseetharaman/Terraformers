{
    "id": "2e3a00d5-88c5-47a4-a608-a20b0111f438",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_character1_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5da8e549-c330-4a9c-9e44-530c24a58170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e3a00d5-88c5-47a4-a608-a20b0111f438",
            "compositeImage": {
                "id": "432e04cf-bdab-4197-955f-6e3a4fb7b5a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5da8e549-c330-4a9c-9e44-530c24a58170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96b5663a-97c9-47dd-ae5f-f3802c730c83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5da8e549-c330-4a9c-9e44-530c24a58170",
                    "LayerId": "fc7c10d7-e992-4dfe-84ba-15d20943d7d9"
                }
            ]
        },
        {
            "id": "c4f4e7d5-0173-4664-af19-98ff5137fbf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e3a00d5-88c5-47a4-a608-a20b0111f438",
            "compositeImage": {
                "id": "2922b47f-03f8-472a-94b0-74c06ddd58be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4f4e7d5-0173-4664-af19-98ff5137fbf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "380d2104-822f-46c2-b43b-e2ae94ca9db2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4f4e7d5-0173-4664-af19-98ff5137fbf3",
                    "LayerId": "fc7c10d7-e992-4dfe-84ba-15d20943d7d9"
                }
            ]
        },
        {
            "id": "6d19f4c6-7ae1-4bcf-885a-6a8834f5b622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e3a00d5-88c5-47a4-a608-a20b0111f438",
            "compositeImage": {
                "id": "d1341d16-ffe7-4908-a340-d2104217e340",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d19f4c6-7ae1-4bcf-885a-6a8834f5b622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79d9d3a8-f592-408a-afc1-2a5c29e9bfe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d19f4c6-7ae1-4bcf-885a-6a8834f5b622",
                    "LayerId": "fc7c10d7-e992-4dfe-84ba-15d20943d7d9"
                }
            ]
        },
        {
            "id": "66e880ca-bdcc-467d-8ed8-f00f2de74ab3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e3a00d5-88c5-47a4-a608-a20b0111f438",
            "compositeImage": {
                "id": "e931e7f3-c658-4131-a711-13123755c232",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66e880ca-bdcc-467d-8ed8-f00f2de74ab3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6c38a75-ebef-48bb-a7f5-32b07661e5fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66e880ca-bdcc-467d-8ed8-f00f2de74ab3",
                    "LayerId": "fc7c10d7-e992-4dfe-84ba-15d20943d7d9"
                }
            ]
        },
        {
            "id": "25b2fe42-7ec8-4eac-9558-fabb680fea1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e3a00d5-88c5-47a4-a608-a20b0111f438",
            "compositeImage": {
                "id": "3739cacc-0f04-45cb-8d0d-e32a66cc714e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25b2fe42-7ec8-4eac-9558-fabb680fea1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "271d6fd8-615b-452d-bd4e-7d5c2d28bf10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25b2fe42-7ec8-4eac-9558-fabb680fea1b",
                    "LayerId": "fc7c10d7-e992-4dfe-84ba-15d20943d7d9"
                }
            ]
        },
        {
            "id": "abaecbbc-8930-4ac5-b51a-bb262caeec7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e3a00d5-88c5-47a4-a608-a20b0111f438",
            "compositeImage": {
                "id": "a8644d3c-d653-4d70-9665-0aa1620adaef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abaecbbc-8930-4ac5-b51a-bb262caeec7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb997a39-a8d2-44ce-9886-fd495c8e7309",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abaecbbc-8930-4ac5-b51a-bb262caeec7f",
                    "LayerId": "fc7c10d7-e992-4dfe-84ba-15d20943d7d9"
                }
            ]
        },
        {
            "id": "184537e0-45e9-4758-807c-f8345b192308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e3a00d5-88c5-47a4-a608-a20b0111f438",
            "compositeImage": {
                "id": "f7b60d3f-4e34-4b30-899e-613c1f85dac4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "184537e0-45e9-4758-807c-f8345b192308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e026aa1a-b7fd-4135-9b52-b85d594fd4af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "184537e0-45e9-4758-807c-f8345b192308",
                    "LayerId": "fc7c10d7-e992-4dfe-84ba-15d20943d7d9"
                }
            ]
        },
        {
            "id": "823a9c5c-ecdf-4f2f-8798-1f2af38efba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e3a00d5-88c5-47a4-a608-a20b0111f438",
            "compositeImage": {
                "id": "3864f47e-3b8a-4df9-9fb2-91b349bba4d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "823a9c5c-ecdf-4f2f-8798-1f2af38efba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a6b48b7-0642-49ba-b3b4-58da156ae389",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "823a9c5c-ecdf-4f2f-8798-1f2af38efba3",
                    "LayerId": "fc7c10d7-e992-4dfe-84ba-15d20943d7d9"
                }
            ]
        },
        {
            "id": "0e22137a-6c07-4f6d-b133-7bd70f74dd84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e3a00d5-88c5-47a4-a608-a20b0111f438",
            "compositeImage": {
                "id": "a4b22ecc-c3f5-4676-8a12-601a08f02584",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e22137a-6c07-4f6d-b133-7bd70f74dd84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6e3eb1c-4941-4637-acc6-cd3bd6d5b9dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e22137a-6c07-4f6d-b133-7bd70f74dd84",
                    "LayerId": "fc7c10d7-e992-4dfe-84ba-15d20943d7d9"
                }
            ]
        },
        {
            "id": "a88cf254-81a6-42e6-96d2-329d7b696936",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e3a00d5-88c5-47a4-a608-a20b0111f438",
            "compositeImage": {
                "id": "4cff6688-2b6e-44c7-84b3-ca04dc143fe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a88cf254-81a6-42e6-96d2-329d7b696936",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0226a1e3-3a6b-4524-bdb9-1e08374eb864",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a88cf254-81a6-42e6-96d2-329d7b696936",
                    "LayerId": "fc7c10d7-e992-4dfe-84ba-15d20943d7d9"
                }
            ]
        },
        {
            "id": "cb927fa0-b1e6-4428-a068-b6873b83b599",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e3a00d5-88c5-47a4-a608-a20b0111f438",
            "compositeImage": {
                "id": "380c8a3d-936a-4607-863d-c8f1e3ef7f86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb927fa0-b1e6-4428-a068-b6873b83b599",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "178b021e-3ef9-400c-a8a0-8c0742ab266f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb927fa0-b1e6-4428-a068-b6873b83b599",
                    "LayerId": "fc7c10d7-e992-4dfe-84ba-15d20943d7d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fc7c10d7-e992-4dfe-84ba-15d20943d7d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e3a00d5-88c5-47a4-a608-a20b0111f438",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}