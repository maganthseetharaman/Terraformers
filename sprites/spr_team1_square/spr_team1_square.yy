{
    "id": "237279ef-99c6-483e-885d-a3ba17004a38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_team1_square",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b61bd65a-97ea-4e03-a336-982c70e14dda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "237279ef-99c6-483e-885d-a3ba17004a38",
            "compositeImage": {
                "id": "c6d99c1f-24a1-4bcd-a5f2-62c12996c6b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b61bd65a-97ea-4e03-a336-982c70e14dda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "544a16b8-453a-464d-a5b8-e112c7849cee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b61bd65a-97ea-4e03-a336-982c70e14dda",
                    "LayerId": "c298ead7-5785-48ec-853e-e7d11de65d2d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c298ead7-5785-48ec-853e-e7d11de65d2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "237279ef-99c6-483e-885d-a3ba17004a38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}