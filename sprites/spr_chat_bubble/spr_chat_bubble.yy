{
    "id": "1aa8106f-82ee-4a79-ab7b-80ce1fa6b00b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_chat_bubble",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e47a7e2b-eb56-4aca-80f4-3b83b1150b8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1aa8106f-82ee-4a79-ab7b-80ce1fa6b00b",
            "compositeImage": {
                "id": "720431ff-e577-4577-9274-87855e7a81ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e47a7e2b-eb56-4aca-80f4-3b83b1150b8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39ff15d5-8025-49d3-aa7b-e8256d19b98a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e47a7e2b-eb56-4aca-80f4-3b83b1150b8b",
                    "LayerId": "81dfb301-6efe-4363-8e8b-b3cc43814a13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "81dfb301-6efe-4363-8e8b-b3cc43814a13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1aa8106f-82ee-4a79-ab7b-80ce1fa6b00b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 8,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 127,
    "yorig": 63
}