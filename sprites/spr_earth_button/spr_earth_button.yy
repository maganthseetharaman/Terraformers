{
    "id": "b638dbb2-7678-4a55-9a6b-c4e7e75c6646",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_earth_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "799c63ac-9cdb-49c0-8903-3ebd41995dcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b638dbb2-7678-4a55-9a6b-c4e7e75c6646",
            "compositeImage": {
                "id": "ceeb4984-11bf-46f6-ac3d-8e3a3116edc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "799c63ac-9cdb-49c0-8903-3ebd41995dcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fce38aa1-6e6c-4711-9240-cadd5c9bb4c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "799c63ac-9cdb-49c0-8903-3ebd41995dcd",
                    "LayerId": "4ef59ff0-d817-4128-b01d-c21c2ac1451b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4ef59ff0-d817-4128-b01d-c21c2ac1451b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b638dbb2-7678-4a55-9a6b-c4e7e75c6646",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}