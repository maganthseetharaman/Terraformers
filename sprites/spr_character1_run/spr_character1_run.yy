{
    "id": "4db71e38-43fa-459d-b726-4dce52fa853e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_character1_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d5df397-9863-458c-a9c5-baf693c7ada0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db71e38-43fa-459d-b726-4dce52fa853e",
            "compositeImage": {
                "id": "84fb9e94-f89f-420e-9ec7-eab4851ca7fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d5df397-9863-458c-a9c5-baf693c7ada0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eebae7b5-fa28-4d6c-83c1-34d6fea9122d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d5df397-9863-458c-a9c5-baf693c7ada0",
                    "LayerId": "f5912b4e-abcd-42b4-8132-820e11adbb17"
                }
            ]
        },
        {
            "id": "ef7b1f6d-0d02-4d3c-a6e2-b9333bf0b1db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db71e38-43fa-459d-b726-4dce52fa853e",
            "compositeImage": {
                "id": "bcceffb7-8e36-445f-a34b-ae5d5876c689",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef7b1f6d-0d02-4d3c-a6e2-b9333bf0b1db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d21b015-a3c1-4244-afbe-066177a6e847",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef7b1f6d-0d02-4d3c-a6e2-b9333bf0b1db",
                    "LayerId": "f5912b4e-abcd-42b4-8132-820e11adbb17"
                }
            ]
        },
        {
            "id": "247f5526-cfef-41aa-8f43-4038ff009873",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db71e38-43fa-459d-b726-4dce52fa853e",
            "compositeImage": {
                "id": "675970fe-12c1-471f-aa8c-aff3d97d7c7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "247f5526-cfef-41aa-8f43-4038ff009873",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ac7f6a2-4eaf-4a6b-a33d-ab3f81ee295a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "247f5526-cfef-41aa-8f43-4038ff009873",
                    "LayerId": "f5912b4e-abcd-42b4-8132-820e11adbb17"
                }
            ]
        },
        {
            "id": "02bc6adc-a8ad-44ba-93cd-190bad2ccb88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db71e38-43fa-459d-b726-4dce52fa853e",
            "compositeImage": {
                "id": "b043140b-f077-429a-a19a-28bbc7c1f06a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02bc6adc-a8ad-44ba-93cd-190bad2ccb88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b48e038-8cc3-4ce5-8281-195e3b6dbeb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02bc6adc-a8ad-44ba-93cd-190bad2ccb88",
                    "LayerId": "f5912b4e-abcd-42b4-8132-820e11adbb17"
                }
            ]
        },
        {
            "id": "1ec176cc-ddf4-4db7-95da-265bf6001342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db71e38-43fa-459d-b726-4dce52fa853e",
            "compositeImage": {
                "id": "7876a778-3bc1-4bae-aa15-5a8ec5ae3df1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ec176cc-ddf4-4db7-95da-265bf6001342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0961b43-28c1-40af-8d4f-fde39e7d44af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ec176cc-ddf4-4db7-95da-265bf6001342",
                    "LayerId": "f5912b4e-abcd-42b4-8132-820e11adbb17"
                }
            ]
        },
        {
            "id": "00aeb9fe-aaa0-46de-b5a5-79cd505f0806",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db71e38-43fa-459d-b726-4dce52fa853e",
            "compositeImage": {
                "id": "3a4ca136-4b56-44e8-92b0-ec84558fa2d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00aeb9fe-aaa0-46de-b5a5-79cd505f0806",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "839a2ca1-e400-4694-816f-8e280d821908",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00aeb9fe-aaa0-46de-b5a5-79cd505f0806",
                    "LayerId": "f5912b4e-abcd-42b4-8132-820e11adbb17"
                }
            ]
        },
        {
            "id": "2c4ac7e8-edcd-4c0f-a794-9e0d5f3bc825",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db71e38-43fa-459d-b726-4dce52fa853e",
            "compositeImage": {
                "id": "45e8aa97-daf7-4f67-9554-713533f2e75d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c4ac7e8-edcd-4c0f-a794-9e0d5f3bc825",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd3a4409-b83f-4e5b-acc3-3ea68986ecd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c4ac7e8-edcd-4c0f-a794-9e0d5f3bc825",
                    "LayerId": "f5912b4e-abcd-42b4-8132-820e11adbb17"
                }
            ]
        },
        {
            "id": "764b8a50-120e-42de-90b2-08cc24a7fbca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db71e38-43fa-459d-b726-4dce52fa853e",
            "compositeImage": {
                "id": "84354fd1-6bd1-4196-baa7-fe6be9271dde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "764b8a50-120e-42de-90b2-08cc24a7fbca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "580300c2-ffd8-4942-a972-151d2ff1e39a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "764b8a50-120e-42de-90b2-08cc24a7fbca",
                    "LayerId": "f5912b4e-abcd-42b4-8132-820e11adbb17"
                }
            ]
        },
        {
            "id": "b3267493-6ec2-418f-84ba-40717b6b7f5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db71e38-43fa-459d-b726-4dce52fa853e",
            "compositeImage": {
                "id": "14d1d87a-317f-4e7d-ae62-3cec5eadb230",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3267493-6ec2-418f-84ba-40717b6b7f5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddb45fe6-2a2b-4dd8-a9b6-0f40f732c8a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3267493-6ec2-418f-84ba-40717b6b7f5a",
                    "LayerId": "f5912b4e-abcd-42b4-8132-820e11adbb17"
                }
            ]
        },
        {
            "id": "09e4c94d-3d4b-4b2d-b5b9-c83506965cbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db71e38-43fa-459d-b726-4dce52fa853e",
            "compositeImage": {
                "id": "d8e155d7-71fc-47cc-b888-22d52c7821c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09e4c94d-3d4b-4b2d-b5b9-c83506965cbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3654b3d0-5398-4df1-bb32-910597a9f9f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09e4c94d-3d4b-4b2d-b5b9-c83506965cbf",
                    "LayerId": "f5912b4e-abcd-42b4-8132-820e11adbb17"
                }
            ]
        },
        {
            "id": "3f16d19a-da88-4bf4-a3f2-b6fbabb099bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db71e38-43fa-459d-b726-4dce52fa853e",
            "compositeImage": {
                "id": "512421be-c945-43f5-b3da-50dfa2db0dc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f16d19a-da88-4bf4-a3f2-b6fbabb099bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a06a27fa-9229-48a5-b3ad-548987060525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f16d19a-da88-4bf4-a3f2-b6fbabb099bb",
                    "LayerId": "f5912b4e-abcd-42b4-8132-820e11adbb17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f5912b4e-abcd-42b4-8132-820e11adbb17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4db71e38-43fa-459d-b726-4dce52fa853e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}