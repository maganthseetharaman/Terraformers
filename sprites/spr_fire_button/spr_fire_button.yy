{
    "id": "ba01b4a4-d645-4617-a65f-1cee92b58d2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fire_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f04a50f-aec8-46f8-b159-657977dda4e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba01b4a4-d645-4617-a65f-1cee92b58d2b",
            "compositeImage": {
                "id": "0e605e6b-70df-4a66-9780-2b08fabeef8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f04a50f-aec8-46f8-b159-657977dda4e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0652ecd-6ebf-4759-babe-5932ffedda3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f04a50f-aec8-46f8-b159-657977dda4e7",
                    "LayerId": "49a5aecc-3bd4-448b-9f23-736a7455fe29"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "49a5aecc-3bd4-448b-9f23-736a7455fe29",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba01b4a4-d645-4617-a65f-1cee92b58d2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}