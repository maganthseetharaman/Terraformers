{
    "id": "c1da15cf-3632-49c8-b185-4edfbde4367e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 101,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "319aeac2-f1f3-4644-a8b5-552283942b50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1da15cf-3632-49c8-b185-4edfbde4367e",
            "compositeImage": {
                "id": "ae79639c-ede9-451d-8285-75aade23bec7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "319aeac2-f1f3-4644-a8b5-552283942b50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "402545cb-2b38-4e01-a5ac-dea6c9523c9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "319aeac2-f1f3-4644-a8b5-552283942b50",
                    "LayerId": "74094301-df40-43e2-88d1-9cc476478bce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "74094301-df40-43e2-88d1-9cc476478bce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1da15cf-3632-49c8-b185-4edfbde4367e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 102,
    "xorig": 0,
    "yorig": 0
}