{
    "id": "f06a6f97-83c2-4591-b83c-87719943f955",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_textbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24cf5ce2-998a-4974-94ff-d0ec6fc412cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f06a6f97-83c2-4591-b83c-87719943f955",
            "compositeImage": {
                "id": "11f915f0-fcea-4220-b04b-f3e59391d790",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24cf5ce2-998a-4974-94ff-d0ec6fc412cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ef82bf9-b9a0-44d2-a476-c27f1356d31c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24cf5ce2-998a-4974-94ff-d0ec6fc412cb",
                    "LayerId": "86183587-1160-4b55-9963-da6d034e2363"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "86183587-1160-4b55-9963-da6d034e2363",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f06a6f97-83c2-4591-b83c-87719943f955",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}