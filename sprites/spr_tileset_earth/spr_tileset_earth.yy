{
    "id": "4a7d551b-7394-4489-a30d-cec04ff8b607",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_earth",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4de13cd4-4d94-4b52-94e8-5d16b71372a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a7d551b-7394-4489-a30d-cec04ff8b607",
            "compositeImage": {
                "id": "4a897f40-3b17-42a9-9c6e-328ee9cf45a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4de13cd4-4d94-4b52-94e8-5d16b71372a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdba66ad-1f52-431c-97a5-2619abe4270a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4de13cd4-4d94-4b52-94e8-5d16b71372a6",
                    "LayerId": "f2848600-5fef-426f-8efb-d3cb698b842f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "f2848600-5fef-426f-8efb-d3cb698b842f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a7d551b-7394-4489-a30d-cec04ff8b607",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}