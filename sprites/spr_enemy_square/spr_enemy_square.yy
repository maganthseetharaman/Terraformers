{
    "id": "fad4de05-f9d3-4166-bf3f-1cd0da6385e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_square",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e8f4fbb-9a90-4a8e-bc0e-44d8e01b76c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fad4de05-f9d3-4166-bf3f-1cd0da6385e0",
            "compositeImage": {
                "id": "2b7c663b-3f7d-4fad-9dd6-39971e4a1c32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e8f4fbb-9a90-4a8e-bc0e-44d8e01b76c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30582945-9db6-47cf-a7e1-620959f3f90f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e8f4fbb-9a90-4a8e-bc0e-44d8e01b76c6",
                    "LayerId": "afedc20c-b6ca-44d7-ba70-8f5643e69944"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "afedc20c-b6ca-44d7-ba70-8f5643e69944",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fad4de05-f9d3-4166-bf3f-1cd0da6385e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}