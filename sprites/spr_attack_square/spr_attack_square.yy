{
    "id": "d746718f-f068-45ea-9872-acfb7a713c23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_attack_square",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88d72a9b-823b-4e5c-a2ad-55e8945889c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d746718f-f068-45ea-9872-acfb7a713c23",
            "compositeImage": {
                "id": "9e675637-5559-42ce-9073-53a5e48e30b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88d72a9b-823b-4e5c-a2ad-55e8945889c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a50ac3d5-bbc5-4306-a441-a4b57f35b295",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88d72a9b-823b-4e5c-a2ad-55e8945889c9",
                    "LayerId": "0d60fd4c-0e80-4758-9822-0d3bc1cb42b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0d60fd4c-0e80-4758-9822-0d3bc1cb42b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d746718f-f068-45ea-9872-acfb7a713c23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 50,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}