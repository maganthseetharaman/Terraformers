{
    "id": "652cc66b-87c2-49b0-bf7f-d3219473a173",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guide_character_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 67,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f53f63e3-930a-4abc-ad9e-6c9dc7bbee4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "652cc66b-87c2-49b0-bf7f-d3219473a173",
            "compositeImage": {
                "id": "1237a958-d02e-4f9b-874b-a4f9c7deba90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f53f63e3-930a-4abc-ad9e-6c9dc7bbee4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e549e03-65e2-4b4e-9c9b-29d005e9b16a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f53f63e3-930a-4abc-ad9e-6c9dc7bbee4d",
                    "LayerId": "3d08b128-7857-47c9-bed5-01d58f474007"
                }
            ]
        },
        {
            "id": "7a3547ae-2984-4149-8849-57a5f2ea04e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "652cc66b-87c2-49b0-bf7f-d3219473a173",
            "compositeImage": {
                "id": "63c90881-2ebe-43fd-b404-049cf4036312",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a3547ae-2984-4149-8849-57a5f2ea04e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52549d42-b370-48ca-9adc-1311020f4818",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a3547ae-2984-4149-8849-57a5f2ea04e2",
                    "LayerId": "3d08b128-7857-47c9-bed5-01d58f474007"
                }
            ]
        },
        {
            "id": "804e2ea5-234c-46d8-af13-105de4d9d8f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "652cc66b-87c2-49b0-bf7f-d3219473a173",
            "compositeImage": {
                "id": "ce27d92e-c5b2-4c4d-9c94-765644ac3e66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "804e2ea5-234c-46d8-af13-105de4d9d8f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdf4f306-4e02-4f1d-8145-ac295a62c7be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "804e2ea5-234c-46d8-af13-105de4d9d8f2",
                    "LayerId": "3d08b128-7857-47c9-bed5-01d58f474007"
                }
            ]
        },
        {
            "id": "255e9520-d057-4563-8d72-6a7ff74dfb47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "652cc66b-87c2-49b0-bf7f-d3219473a173",
            "compositeImage": {
                "id": "eda01ac5-724a-4d7e-99d8-d7f8b3caa80e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "255e9520-d057-4563-8d72-6a7ff74dfb47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caad7b82-1cf5-4a15-bebf-84a1d12d9ad2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "255e9520-d057-4563-8d72-6a7ff74dfb47",
                    "LayerId": "3d08b128-7857-47c9-bed5-01d58f474007"
                }
            ]
        },
        {
            "id": "306e0ca8-a6a9-4d43-8482-b7cc908a4546",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "652cc66b-87c2-49b0-bf7f-d3219473a173",
            "compositeImage": {
                "id": "77714b54-4780-40b1-bece-947de2ad9f82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "306e0ca8-a6a9-4d43-8482-b7cc908a4546",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4e2c433-7f81-4ca4-9c45-a59dc609d309",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "306e0ca8-a6a9-4d43-8482-b7cc908a4546",
                    "LayerId": "3d08b128-7857-47c9-bed5-01d58f474007"
                }
            ]
        },
        {
            "id": "a25f8f52-12b7-4d9f-b026-be0903a47d5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "652cc66b-87c2-49b0-bf7f-d3219473a173",
            "compositeImage": {
                "id": "ae5053fa-f6c3-4105-8189-9cdcb7f92950",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a25f8f52-12b7-4d9f-b026-be0903a47d5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38a3e9e9-a1d4-46ff-bb03-bb5e6805b03f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a25f8f52-12b7-4d9f-b026-be0903a47d5a",
                    "LayerId": "3d08b128-7857-47c9-bed5-01d58f474007"
                }
            ]
        },
        {
            "id": "f0abfdc4-5e3f-4223-ae5b-0f326ed29c58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "652cc66b-87c2-49b0-bf7f-d3219473a173",
            "compositeImage": {
                "id": "11ec0b3f-4fe5-4b92-95a9-8089fc0ab318",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0abfdc4-5e3f-4223-ae5b-0f326ed29c58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e5931cd-9b85-45cb-b96d-8f3299a745ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0abfdc4-5e3f-4223-ae5b-0f326ed29c58",
                    "LayerId": "3d08b128-7857-47c9-bed5-01d58f474007"
                }
            ]
        },
        {
            "id": "e84a001b-5479-404c-9d50-b9dcc66d32c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "652cc66b-87c2-49b0-bf7f-d3219473a173",
            "compositeImage": {
                "id": "5629d8f1-70a1-4aa5-b891-059ee16e52d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e84a001b-5479-404c-9d50-b9dcc66d32c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4532c89-81a1-417f-985e-0e9146cdc9c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e84a001b-5479-404c-9d50-b9dcc66d32c5",
                    "LayerId": "3d08b128-7857-47c9-bed5-01d58f474007"
                }
            ]
        },
        {
            "id": "c41e1f5f-f1b7-4eaf-9555-bfd26ee2c554",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "652cc66b-87c2-49b0-bf7f-d3219473a173",
            "compositeImage": {
                "id": "0bf80d02-4f8d-424b-bb7d-17508c439e86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c41e1f5f-f1b7-4eaf-9555-bfd26ee2c554",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caafc2e6-7917-4b63-86b5-28152fa283e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c41e1f5f-f1b7-4eaf-9555-bfd26ee2c554",
                    "LayerId": "3d08b128-7857-47c9-bed5-01d58f474007"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 68,
    "layers": [
        {
            "id": "3d08b128-7857-47c9-bed5-01d58f474007",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "652cc66b-87c2-49b0-bf7f-d3219473a173",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}