{
    "id": "96880c5f-ba51-4e48-bdf4-b6f3222c4986",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_water_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c33cd460-6538-4ae1-b833-8b45bfd1c2ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96880c5f-ba51-4e48-bdf4-b6f3222c4986",
            "compositeImage": {
                "id": "038b0630-cee1-45eb-b709-a587265f383c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c33cd460-6538-4ae1-b833-8b45bfd1c2ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3a1ddb6-a647-45fd-a4fa-d85229e5ec37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c33cd460-6538-4ae1-b833-8b45bfd1c2ba",
                    "LayerId": "26273be8-ca7e-49b1-9660-d4c04b233555"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "26273be8-ca7e-49b1-9660-d4c04b233555",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96880c5f-ba51-4e48-bdf4-b6f3222c4986",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}