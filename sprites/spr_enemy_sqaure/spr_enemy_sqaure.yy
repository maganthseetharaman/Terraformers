{
    "id": "6db8d36e-4066-48e7-8410-c219f415bf73",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_sqaure",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fecc43e7-e6e4-4fb7-a0e7-2ddde005d1f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6db8d36e-4066-48e7-8410-c219f415bf73",
            "compositeImage": {
                "id": "4809fc6f-3d13-4a66-9428-7375469102c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fecc43e7-e6e4-4fb7-a0e7-2ddde005d1f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b40bee3-abab-4719-9db9-f186dad287bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fecc43e7-e6e4-4fb7-a0e7-2ddde005d1f6",
                    "LayerId": "2c41e9a0-a7be-457d-9d0a-ff46d05b6a65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "2c41e9a0-a7be-457d-9d0a-ff46d05b6a65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6db8d36e-4066-48e7-8410-c219f415bf73",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}