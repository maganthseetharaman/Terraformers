{
    "id": "527e5db4-dcc0-4780-b3eb-232c4466b132",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ocean_tile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60c805b2-8691-461b-8890-3d538468c91c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "527e5db4-dcc0-4780-b3eb-232c4466b132",
            "compositeImage": {
                "id": "519afb45-112e-4622-92ef-e75bd225db0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60c805b2-8691-461b-8890-3d538468c91c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71ef971a-1011-4eee-a819-40d8242499ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60c805b2-8691-461b-8890-3d538468c91c",
                    "LayerId": "11a8a6ea-330f-45e2-bdb3-b64037d8b4c8"
                }
            ]
        },
        {
            "id": "229a5924-732e-4272-8c60-333f905a3016",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "527e5db4-dcc0-4780-b3eb-232c4466b132",
            "compositeImage": {
                "id": "a675c0e8-7035-48f8-8174-11c16acd8396",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "229a5924-732e-4272-8c60-333f905a3016",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e56c9cae-d67a-41dd-8f43-cd56268d2619",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "229a5924-732e-4272-8c60-333f905a3016",
                    "LayerId": "11a8a6ea-330f-45e2-bdb3-b64037d8b4c8"
                }
            ]
        },
        {
            "id": "41a4d12e-36c3-4a54-a3f9-69c0d9bfa084",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "527e5db4-dcc0-4780-b3eb-232c4466b132",
            "compositeImage": {
                "id": "20fa994a-996d-4372-9b7a-8c6e51590a52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41a4d12e-36c3-4a54-a3f9-69c0d9bfa084",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc7713ba-db5f-43a3-9eb4-e43703c3b622",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41a4d12e-36c3-4a54-a3f9-69c0d9bfa084",
                    "LayerId": "11a8a6ea-330f-45e2-bdb3-b64037d8b4c8"
                }
            ]
        },
        {
            "id": "e6e4205e-2b6b-4dae-ad79-80d94b05e110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "527e5db4-dcc0-4780-b3eb-232c4466b132",
            "compositeImage": {
                "id": "e55889af-c3e7-4e3b-8a1b-b7c0ddf8ff46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6e4205e-2b6b-4dae-ad79-80d94b05e110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b10768a9-c0d2-43d2-a325-6b1afc267678",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6e4205e-2b6b-4dae-ad79-80d94b05e110",
                    "LayerId": "11a8a6ea-330f-45e2-bdb3-b64037d8b4c8"
                }
            ]
        },
        {
            "id": "bf3e8d53-25f2-48ab-9735-a13ae8cfecb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "527e5db4-dcc0-4780-b3eb-232c4466b132",
            "compositeImage": {
                "id": "a06ddae1-c398-4995-9b5d-0c7131481b1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf3e8d53-25f2-48ab-9735-a13ae8cfecb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92b2bea7-df88-416e-867d-bf071b5d84fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf3e8d53-25f2-48ab-9735-a13ae8cfecb8",
                    "LayerId": "11a8a6ea-330f-45e2-bdb3-b64037d8b4c8"
                }
            ]
        },
        {
            "id": "0d0f0e67-8512-4dc0-ac9d-77a1b6b81276",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "527e5db4-dcc0-4780-b3eb-232c4466b132",
            "compositeImage": {
                "id": "bd6a0b75-b4fb-40e7-84ed-ee0a34a874d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d0f0e67-8512-4dc0-ac9d-77a1b6b81276",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c370f03e-db43-4c11-9a3c-fac26d1ea714",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d0f0e67-8512-4dc0-ac9d-77a1b6b81276",
                    "LayerId": "11a8a6ea-330f-45e2-bdb3-b64037d8b4c8"
                }
            ]
        },
        {
            "id": "729d9f94-91c7-44c7-9d9f-66fb65225c9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "527e5db4-dcc0-4780-b3eb-232c4466b132",
            "compositeImage": {
                "id": "ca838801-6c2f-4ce7-b6db-4ac8848a9a2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "729d9f94-91c7-44c7-9d9f-66fb65225c9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb13e7d3-e10d-4b13-8659-566c6fd421d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "729d9f94-91c7-44c7-9d9f-66fb65225c9f",
                    "LayerId": "11a8a6ea-330f-45e2-bdb3-b64037d8b4c8"
                }
            ]
        },
        {
            "id": "24843257-2bf1-4ac8-b93a-18d0aa7d2f8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "527e5db4-dcc0-4780-b3eb-232c4466b132",
            "compositeImage": {
                "id": "28d0dbdb-e9ab-46df-b38d-fda99707c373",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24843257-2bf1-4ac8-b93a-18d0aa7d2f8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "279482f5-bd98-4b16-9fb7-05353f2387aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24843257-2bf1-4ac8-b93a-18d0aa7d2f8d",
                    "LayerId": "11a8a6ea-330f-45e2-bdb3-b64037d8b4c8"
                }
            ]
        },
        {
            "id": "c766e887-3174-49cf-a54b-7b277ad12c96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "527e5db4-dcc0-4780-b3eb-232c4466b132",
            "compositeImage": {
                "id": "c8b225c6-ceeb-44d9-addc-e7fb0e00b590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c766e887-3174-49cf-a54b-7b277ad12c96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b6da5cb-3b3b-43cf-a494-d4d3e87755d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c766e887-3174-49cf-a54b-7b277ad12c96",
                    "LayerId": "11a8a6ea-330f-45e2-bdb3-b64037d8b4c8"
                }
            ]
        },
        {
            "id": "6b22d092-048b-4f9a-8bc7-65883f67ef9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "527e5db4-dcc0-4780-b3eb-232c4466b132",
            "compositeImage": {
                "id": "3e953e5d-92c3-46e0-b41d-323b5c7cf81a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b22d092-048b-4f9a-8bc7-65883f67ef9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bd2348d-c270-4d40-a58e-773cb7407d10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b22d092-048b-4f9a-8bc7-65883f67ef9a",
                    "LayerId": "11a8a6ea-330f-45e2-bdb3-b64037d8b4c8"
                }
            ]
        },
        {
            "id": "2a17009f-5f6a-476b-83de-0955801bc751",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "527e5db4-dcc0-4780-b3eb-232c4466b132",
            "compositeImage": {
                "id": "df514035-7a62-46cd-83d3-96e1fec08348",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a17009f-5f6a-476b-83de-0955801bc751",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee774dbb-177b-4f96-b0b8-208742025c76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a17009f-5f6a-476b-83de-0955801bc751",
                    "LayerId": "11a8a6ea-330f-45e2-bdb3-b64037d8b4c8"
                }
            ]
        },
        {
            "id": "2745efb5-e9e7-4b2f-b312-2c003bfcc732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "527e5db4-dcc0-4780-b3eb-232c4466b132",
            "compositeImage": {
                "id": "9df3c08a-2c70-44d2-9e83-8a35fb6509d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2745efb5-e9e7-4b2f-b312-2c003bfcc732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16d40ba3-bdbe-48e0-86ab-1ba8e1f79067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2745efb5-e9e7-4b2f-b312-2c003bfcc732",
                    "LayerId": "11a8a6ea-330f-45e2-bdb3-b64037d8b4c8"
                }
            ]
        },
        {
            "id": "82c41e97-7e4a-4497-9e36-6d34a2f3e6d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "527e5db4-dcc0-4780-b3eb-232c4466b132",
            "compositeImage": {
                "id": "6d67f265-5d89-4ac1-98b3-17e66d439209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82c41e97-7e4a-4497-9e36-6d34a2f3e6d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "498f26a2-237b-4491-a6e5-01f3114f482f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82c41e97-7e4a-4497-9e36-6d34a2f3e6d9",
                    "LayerId": "11a8a6ea-330f-45e2-bdb3-b64037d8b4c8"
                }
            ]
        },
        {
            "id": "7ca20130-4c50-40e6-84cd-d8f60dccdb35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "527e5db4-dcc0-4780-b3eb-232c4466b132",
            "compositeImage": {
                "id": "e6177709-b8df-47bc-92c0-67d805a162ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ca20130-4c50-40e6-84cd-d8f60dccdb35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a73f4701-33f2-40a6-90fc-7d7686cdb87b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ca20130-4c50-40e6-84cd-d8f60dccdb35",
                    "LayerId": "11a8a6ea-330f-45e2-bdb3-b64037d8b4c8"
                }
            ]
        },
        {
            "id": "8a8a5bc9-5659-48bf-86ae-07629266ce96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "527e5db4-dcc0-4780-b3eb-232c4466b132",
            "compositeImage": {
                "id": "15e21bb2-a6de-4386-8e70-26b865bda215",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a8a5bc9-5659-48bf-86ae-07629266ce96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64c32daf-339d-4ed2-b87b-062705736fa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a8a5bc9-5659-48bf-86ae-07629266ce96",
                    "LayerId": "11a8a6ea-330f-45e2-bdb3-b64037d8b4c8"
                }
            ]
        },
        {
            "id": "cdcdb9e3-c626-448f-bc78-49e756c8d883",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "527e5db4-dcc0-4780-b3eb-232c4466b132",
            "compositeImage": {
                "id": "e79fdd32-5e5f-4257-8f35-3bdce696c546",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdcdb9e3-c626-448f-bc78-49e756c8d883",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45600621-28ff-4639-8fc9-bf30d701553a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdcdb9e3-c626-448f-bc78-49e756c8d883",
                    "LayerId": "11a8a6ea-330f-45e2-bdb3-b64037d8b4c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "11a8a6ea-330f-45e2-bdb3-b64037d8b4c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "527e5db4-dcc0-4780-b3eb-232c4466b132",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}