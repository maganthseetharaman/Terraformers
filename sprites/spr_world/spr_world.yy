{
    "id": "ae6fa654-1678-48e8-b224-6b6cbc833845",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_world",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9c82f55-5a18-4ef5-8655-35a3516177df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae6fa654-1678-48e8-b224-6b6cbc833845",
            "compositeImage": {
                "id": "b19f8c89-3221-474d-b4c4-175f46b2d6b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9c82f55-5a18-4ef5-8655-35a3516177df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c45e1ea-1eba-42d6-b1e0-4342a2ad9c33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9c82f55-5a18-4ef5-8655-35a3516177df",
                    "LayerId": "96ea133d-b751-42d3-9101-9cf90996c62f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "96ea133d-b751-42d3-9101-9cf90996c62f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae6fa654-1678-48e8-b224-6b6cbc833845",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}